<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use Config;
use App\Package;
use App\User;
use Carbon\Carbon;
use Cake\Chronos\Chronos;
use App\Traits\CompanyPackageTrait;
use App\Traits\JobSeekerPackageTrait;
/** All Stripe Details class * */
use Stripe\Stripe;
use Stripe\Charge;

class PayuOrderController extends Controller
{

    use CompanyPackageTrait;
    use JobSeekerPackageTrait;

    private $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*         * ****************************************** */
        $this->middleware(function ($request, $next) {
            if (Auth::guard('company')->check()) {
                $this->redirectTo = 'company.home';
            }
            return $next($request);
        });
        /*         * ****************************************** */
    }

    public function payuOrderForm($package_id, $new_or_upgrade)
    {
        $package = Package::findOrFail($package_id);
        $package_price =  $package->package_price;
        
        $buyer_id = '';
        $buyer_name = '';
        $buyer_email = '';
        $buyer_phone = '';
        $u_type = '';
        if (Auth::guard('company')->check()) {
            $buyer_id = Auth::guard('company')->user()->id;
            $buyer_email = Auth::guard('company')->user()->email;
            $buyer_phone = Auth::guard('company')->user()->phone;
            $buyer_name = Auth::guard('company')->user()->name . '(' . Auth::guard('company')->user()->email . ')';
            $u_type = 'companies';
        }
        if (Auth::check()) {
            $buyer_id = Auth::user()->id;
            $buyer_email = Auth::user()->email;
            $buyer_phone = Auth::user()->phone;
            $buyer_name = Auth::user()->getName() . '(' . Auth::user()->email . ')';
            $u_type = 'users';
        }
        
        $MERCHANT_KEY = "bP4rSdFk";//"hDkYGPQe";
        $SALT 		  = "GIUoIF6ZEO";//"yIEkykqEH3";
        $txnid 		  = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $udf1 		  = '';
        $udf2 		  = '';
        $udf3 		  = '';
        $udf4 		  = '';
        $udf5 		  = '';
        $amount 	  = $package_price;//$this->input->post('corporate_plan_rate');
        //$productinfo  = $package_id;
        //$data_pro = array('package_id'=>$package_id,'u_type'=>$u_type,'user_id'=>$buyer_id);
        $productinfo  = $package_id.'@@'.$u_type.'@@'.$buyer_id;
        $fname 		  = $buyer_name;//$this->input->post('user_name');
        $email        = $buyer_email;//$this->input->post('user_email');

        $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|'. $fname . '|' . $email .'|'.$udf1.'|' .$udf2.'|' .$udf3.'|'.$udf4.'|'.$udf5.'||||||'. $SALT;

        $hash = strtolower(hash('sha512', $hashstring));

      
        return view('order.pay_with_payu')
                        ->with('package', $package)
                        ->with('package_id', $package_id)
                        ->with('package_price', $package_price)
                        ->with('mkey' 		, $MERCHANT_KEY)
                        ->with('tid' 		, $txnid)
                        ->with('hash' 		, $hash)
                        ->with('amount'	, $amount)
                        ->with('pinfo' 	, $productinfo)
                        ->with('name' 		, $fname)
                        ->with('productinfo',$productinfo)
                        ->with('mailid' 	, $email)
                        ->with('phoneno'	, $buyer_phone);
    }
    
    public function payuconfirm(Request $request)
    {
        $package_id = $request->package_id;
        //echo "<pre>HERE";print_r($request);die;
        $package = Package::findOrFail($package_id);
        
        $package_price =  $request->package_price;
        
        return view('order.payuconfirmation')
                ->with('package', $package)
                        ->with('package_id', $package_id)
                        ->with('package_price', $package_price);;
       
    }
    
    public function payuconfirm1(Request $request)
    {
       echo "<pre>";print_r($request);
       die;
       return view('order.payuconfirmation');
       
    }
    
   

    /**
     * Store a details of payment with paypal.
     *
     * @param IlluminateHttpRequest $request
     * @return IlluminateHttpResponse
     */
    public function payuOrderPackage(Request $request)
    {
        echo $request->package_id;die;
        $package = Package::findOrFail($request->package_id);

        $order_amount = $package->package_price;

        /*         * ************************ */
        $buyer_id = '';
        $buyer_name = '';
        if (Auth::guard('company')->check()) {
            $buyer_id = Auth::guard('company')->user()->id;
            $buyer_name = Auth::guard('company')->user()->name . '(' . Auth::guard('company')->user()->email . ')';
        }
        if (Auth::check()) {
            $buyer_id = Auth::user()->id;
            $buyer_name = Auth::user()->getName() . '(' . Auth::user()->email . ')';
        }
        $package_for = ($package->package_for == 'employer') ? __('Employer') : __('Job Seeker');
        $description = $package_for . ' ' . $buyer_name . ' - ' . $buyer_id . ' ' . __('Package') . ':' . $package->package_title;
        /*         * ************************ */
        Stripe::setApiKey(Config::get('stripe.stripe_secret'));
        try {
            $charge = Charge::create(array(
                        "amount" => $order_amount * 100,
                        "currency" => "INR",
                        "source" => $request->input('stripeToken'), // obtained with Stripe.js
                        "description" => $description
            ));
            if ($charge['status'] == 'succeeded') {
                /**
                 * Write Here Your Database insert logic.
                 */
                if (Auth::guard('company')->check()) {
                    $company = Auth::guard('company')->user();
                    $this->addCompanyPackage($company, $package);
                }
                if (Auth::check()) {
                    $user = Auth::user();
                    $this->addJobSeekerPackage($user, $package);
                }

                flash(__('You have successfully subscribed to selected package'))->success();
                return Redirect::route($this->redirectTo);
            } else {
                flash(__('Package subscription failed'));
                return Redirect::route($this->redirectTo);
            }
        } catch (Exception $e) {
            flash($e->getMessage());
            return Redirect::route($this->redirectTo);
        }
    }

    
    
    public function success()
    {
        flash(__('You have successfully subscribed to selected package'))->success();
        return Redirect::route($this->redirectTo);
        die;
    }
    
    public function failed()
    {
        flash(__('Package subscription failed'));
        return Redirect::route($this->redirectTo);
        die;
    }
    
    public function PayuOrderUpgradePackage()
    {
        
        
        //echo $package_id;
        echo "<pre>";print_r($request);die;

        $package = Package::findOrFail($package_id);

        $order_amount = $package->package_price;

        /*         * ************************ */
        $buyer_id = '';
        $buyer_name = '';
        if (Auth::guard('company')->check()) {
            $buyer_id = Auth::guard('company')->user()->id;
            $buyer_name = Auth::guard('company')->user()->name . '(' . Auth::guard('company')->user()->email . ')';
        }
        if (Auth::check()) {
            $buyer_id = Auth::user()->id;
            $buyer_name = Auth::user()->getName() . '(' . Auth::user()->email . ')';
        }
        /*         * ************************* */
   //echo "<pre>"; print_r($package);die;
        /*Stripe::setApiKey(Config::get('stripe.stripe_secret'));
        try {
            $charge = Charge::create(array(
                        "amount" => $order_amount * 100,
                        "currency" => "USD",
                        "source" => $request->input('stripeToken'), // obtained with Stripe.js
                        "description" => $description
            ));*/
            //if ($charge['status'] == 'succeeded') {
                /**
                 * Write Here Your Database insert logic.
                 */
                if (Auth::guard('company')->check()) {
                    $company = Auth::guard('company')->user();
                   // echo "<pre>"; print_r($company);die;
                    $this->C($company, $package);
                }
                if (Auth::check()) {
                    $user = Auth::user();
                    $this->updateJobSeekerPackage($user, $package);
                }

                flash(__('You have successfully subscribed to selected package'))->success();
                return Redirect::route($this->redirectTo);
           /* } else {
                flash(__('Package subscription failed'));
                return Redirect::route($this->redirectTo);
            }*/
       /* } catch (Exception $e) {
            flash($e->getMessage());
            return Redirect::route($this->redirectTo);
        }*/
    }

}
