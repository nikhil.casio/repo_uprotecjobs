@extends('layouts.app')

@section('content') 

<!-- Header start --> 

@include('includes.header') 

<!-- Header end --> 

<!-- Inner Page Title start --> 

@include('includes.inner_page_title', ['page_title'=>__('Register')]) 


<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        @include('flash::message')

        

           <div class="useraccountwrap">

                <div class="userccount">

                    <div class="userbtns">

                        <ul class="nav nav-tabs">

                            <?php

                            $c_or_e = old('candidate_or_employer', 'candidate');

                            ?>

                            <li class="nav-item"><a class="nav-link {{($c_or_e == 'candidate')? 'active':''}}" data-toggle="tab" href="#candidate" aria-expanded="true">{{__('Candidate')}}</a></li>

                            <li class="nav-item"><a class="nav-link {{($c_or_e == 'employer')? 'active':''}}" data-toggle="tab" href="#employer" aria-expanded="false">{{__('Employer')}}</a></li>

                        </ul>

                    </div>

                    <div class="tab-content">

                        <div id="candidate" class="formpanel tab-pane {{($c_or_e == 'candidate')? 'active':''}}">

                            <form class="form-horizontal" method="POST" action="{{ route('register') }}" data-parsley-validate>

                                {{ csrf_field() }}

                                <input type="hidden" name="candidate_or_employer" value="candidate" />

                                <div class="formrow{{ $errors->has('first_name') ? ' has-error' : '' }}">

                                    <input type="text" name="first_name" class="form-control isAlpha" required  data-parsley-required-message="Please enter first name" placeholder="{{__('First Name*')}}" value="{{old('first_name')}}">

                                    @if ($errors->has('first_name')) <span class="help-block"> <strong>{{ $errors->first('first_name') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('middle_name') ? ' has-error' : '' }}">

                                    <input type="text" name="middle_name" class="form-control isAlpha" placeholder="{{__('Middle Name')}}" value="{{old('middle_name')}}"   data-parsley-required-message="Please enter middle name" >

                                    @if ($errors->has('middle_name')) <span class="help-block"> <strong>{{ $errors->first('middle_name') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('last_name') ? ' has-error' : '' }}">

                                    <input type="text" name="last_name" class="form-control isAlpha" required="required" placeholder="{{__('Last Name*')}}" value="{{old('last_name')}}" required  data-parsley-required-message="Please enter last name">

                                    @if ($errors->has('last_name')) <span class="help-block"> <strong>{{ $errors->first('last_name') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">

                                    <input type="email" name="email" class="form-control" required="required" placeholder="{{__('Email*')}}" value="{{old('email')}}"  required
                                                   data-parsley-required-message="Please enter email id"
                                                   data-parsley-pattern="/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}/g"
                                                   data-parsley-pattern-message="Invalid email address."
                                                   data-parsley-type="email"
                                                   data-parsley-type-message="Please enter valid email id">

                                    @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <input type="password" id="password" name="password" class="form-control" placeholder="{{__('Password*')}}" value=""  data-parsley-required-message="Please enter password"
                                                   data-parsley-uppercase="1"
                                                   data-parsley-lowercase="1"
                                                   data-parsley-number="1"
                                                   data-parsley-special="1" data-parsley-required autocomplete="false" onKeyUp="checkCandidatePasswordStrength();" >
                                                   
                                                   <div id="candidate-password-strength-status"></div>

                                    @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                                    <input type="password" name="password_confirmation" class="form-control"  placeholder="Enter confirm password" id="confirm_password"
                                                   data-parsley-required-message="Please enter confirm password"
                                                   data-parsley-equalto="#password"
                                                   data-parsley-equalto-message="Confirm password and password must be same."
                                                   data-parsley-required placeholder="{{__('Password Confirmation*')}}" value="">

                                    @if ($errors->has('password_confirmation')) <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span> @endif </div>

                                    

                                    <div class="formrow{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">

    <?php

	$is_checked = '';

	if (old('is_subscribed', 1)) {

		$is_checked = 'checked="checked"';

	}

	?>

                                    

                                    <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />{{__('Subscribe to news letter')}}

                                    @if ($errors->has('is_subscribed')) <span class="help-block"> <strong>{{ $errors->first('is_subscribed') }}</strong> </span> @endif </div>

                                    

                                    

                                <div class="formrow{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">

                                    <input type="checkbox" value="1" name="terms_of_use" />

                                    <a href="{{url('cms/terms-of-use')}}">{{__('I accept Terms of Use')}}</a>



                                    @if ($errors->has('terms_of_use')) <span class="help-block"> <strong>{{ $errors->first('terms_of_use') }}</strong> </span> @endif </div>

                             

                                <input type="submit" class="btn" value="{{__('Register')}}">

                            </form>

                        </div>

                        <div id="employer" class="formpanel tab-pane fade {{($c_or_e == 'employer')? 'active':''}}">

                            <form class="form-horizontal" method="POST" action="{{ route('company.register') }}" data-parsley-validate>

                                {{ csrf_field() }}

                                <input type="hidden" name="candidate_or_employer" value="employer" />

                                <div class="formrow{{ $errors->has('name') ? ' has-error' : '' }}">

                                    <input type="text" name="name" class="form-control" required="required" placeholder="{{__('Company Name*')}}" value="{{old('name')}}" required  data-parsley-required-message="Please enter name" >

                                    @if ($errors->has('name')) <span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span> @endif </div>
                                    
                                <!--<div class="formrow{{ $errors->has('company_name') ? ' has-error' : '' }}">

                                    <input type="text" name="company_name" class="form-control" required="required" placeholder="{{__('Company Name*')}}" value="{{old('company_name')}}" required  data-parsley-required-message="Please enter company name" >

                                    @if ($errors->has('company_name')) <span class="help-block"> <strong>{{ $errors->first('company_name') }}</strong> </span> @endif </div>-->
                                    
                                <div class="formrow{{ $errors->has('gst_no') ? ' has-error' : '' }}">

                                    <input type="text" name="gst_no" class="form-control"  placeholder="{{__('GST Number')}}" value="{{old('gst_no')}}"   
                                    data-parsley-required-message="Please enter GST number">
                                    <!--<input type="text" name="gst_no" class="form-control"  placeholder="{{__('GST Number')}}" value="{{old('gst_no')}}"   
                                    data-parsley-required-message="Please enter GST number" 
                                    data-parsley-pattern="/[0-9]{2}[a-z]{4}([a-z]{1}|[0-9]{1}).[0-9]{3}[a-z]([a-z]|[0-9]){3}/g" 
                                                   data-parsley-pattern-message="Please Enter Valid GSTIN Number">-->

                                    @if ($errors->has('gst_no')) <span class="help-block"> <strong>{{ $errors->first('gst_no') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">

                                    <input type="email" name="email" class="form-control"  placeholder="{{__('Email*')}}" value="{{old('email')}}" required
                                                   data-parsley-required-message="Please enter email id"
                                                   data-parsley-pattern="/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}/g"
                                                   data-parsley-pattern-message="Invalid email address."
                                                   data-parsley-type="email"
                                                   data-parsley-type-message="Please enter valid email id">

                                    @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <input type="password" name="password" class="form-control" id="password_employee" placeholder="Enter password" data-parsley-minlength="8"
                                                   data-parsley-required-message="Please enter password"
                                                   data-parsley-uppercase="1"
                                                   data-parsley-lowercase="1"
                                                   data-parsley-number="1"
                                                   data-parsley-special="1" data-parsley-required autocomplete="false" placeholder="{{__('Password*')}}" value="" onKeyUp="checkEmployPasswordStrength();">
                                         <div id="employee_password_length_status"></div>
                                    @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                                    <input type="password" name="password_confirmation" class="form-control" data-parsley-required-message="Please enter confirm password"
                                                   data-parsley-equalto="#password_employee"
                                                   data-parsley-equalto-message="Confirm password and password must be same."
                                                   data-parsley-required placeholder="{{__('Password Confirmation*')}}" value="">

                                    @if ($errors->has('password_confirmation')) <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span> @endif </div>

                                    <div class="formrow{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">

    <?php

	$is_checked = '';

	if (old('is_subscribed', 1)) {

		$is_checked = 'checked="checked"';

	}

	?>

                                    

                                    <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />{{__('Subscribe to news letter')}}

                                    @if ($errors->has('is_subscribed')) <span class="help-block"> <strong>{{ $errors->first('is_subscribed') }}</strong> </span> @endif </div>

                                <div class="formrow{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">

                                    <input type="checkbox" value="1" name="terms_of_use" />

                                    <a href="{{url('terms-of-use')}}">{{__('I accept Terms of Use')}}</a>



                                    @if ($errors->has('terms_of_use')) <span class="help-block"> <strong>{{ $errors->first('terms_of_use') }}</strong> </span> @endif </div>

                            

                                <input type="submit" class="btn" value="{{__('Register')}}">

                            </form>

                        </div>

                    </div>

                    <!-- sign up form -->

                    <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('Have Account')}}? <a href="{{route('login')}}">{{__('Sign in')}}</a></div>

                    <!-- sign up form end--> 



                </div>

            </div>

        

    </div>

</div>

 <script>
        function checkCandidatePasswordStrength() {
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 6) {
                $('#candidate-password-strength-status').removeClass();
                $('#candidate-password-strength-status').addClass('weak-password');
                $('#candidate-password-strength-status').html("Weak (should be atleast 6 characters.)");
            } else {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                    $('#candidate-password-strength-status').removeClass();
                    $('#candidate-password-strength-status').addClass('strong-password');
                    $('#candidate-password-strength-status').html("Strong");
                } else {
                    $('#candidate-password-strength-status').removeClass();
                    $('#candidate-password-strength-status').addClass('medium-password');
                    $('#candidate-password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }
        
         function checkEmployPasswordStrength() {
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password_employee').val().length < 6) {
                $('#employee_password_length_Status').removeClass();
                $('#employee_password_length_Status').addClass('weak-password');
                $('#employee_password_length_Status').html("Weak (should be atleast 6 characters.)");
            } else {
                if ($('#password_employ').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                    $('#employee_password_length_Status').removeClass();
                    $('#employee_password_length_Status').addClass('strong-password');
                    $('#employee_password_length_Status').html("Strong");
                } else {
                    $('#employee_password_length_Status').removeClass();
                    $('#employee_password_length_Status').addClass('medium-password');
                    $('#employee_password_length_Status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }
    </script>

@include('includes.footer')

@endsection 