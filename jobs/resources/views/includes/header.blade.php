<style>
.nav-contact{
    background:#604aab;
    padding:5px 0;
    color:#fff !important;
}
.nav-contact a{
    color:#fff !important;
}
.nav-contact .navbar-nav>li>a{
    border-bottom:0;
}
.menu-bg{
    background:#604aab;
}
.menu-bg .navbar-light .navbar-nav .active>.nav-link, 
.menu-bg .navbar-light .navbar-nav .nav-link.active, 
.menu-bg .navbar-light .navbar-nav .nav-link.show, 
.menu-bg .navbar-light .navbar-nav .show>.nav-link,
.menu-bg .navbar-light .navbar-nav .nav-link{
    color:#fff;
}
.menu-bg .navbar-nav>li>a{
    border-bottom:0;
}
{
    background:#fd7816;
}
.navbar-expand-lg .navbar-nav .nav-link.register{
    background: #ff6100;
}
    /* COOKIE BOX */
.cookieConsentContainer {
    z-index: 999;
    width: 350px;
    min-height: 20px;
    padding: 30px 30px 30px 30px;
    overflow: hidden;
    position: fixed;
    bottom: 30px;
    left: 30px;
    border-radius: 4px;
    background: #fff;
    border: 1px solid #e4e4e4;
    box-shadow: 2px 3px 4px 0 rgba(0, 0, 0, .05);
    display:none
}
.four-plan .boxes{
    height:490px !important;
}
.cookieConsentContainer .cookieDesc p {
    margin: 0;
    padding: 0;
    display: block;
    margin-top: 10px;
    margin-bottom: 10px;
}

.cookieConsentContainer .cookieButton {
    line-height: 1;
}

@media (max-width: 768px) {
    .cookieConsentContainer {
        bottom: 0 !important;
        left: 0 !important;
        width: 100% !important;
        border-radius: 0 !important;
    }
}
</style>
<div class="header position-sticky">
    <div class="container-fluid">
        <div class="row align-items-center nav-contact d-none d-lg-flex d-xl-flex">
            <div class="col-lg-10 col-md-8 col-12">
                <ul class="nav navbar-nav top_nav flex-row justify-content-end">
                    <li><a href="#"><i class="fa fa-phone"></i> <strong>+91 - 20 - 24578003</strong></a></li>
                    <li><a href="#"><i class="fa fa-mobile"></i> <strong>+91 - 7030920031</strong></a></li>
                    <li><a href="#"><strong><i class="fa fa-envelope"></i> response@uprotecjobs.com</strong></a></li>
            
                </ul>
            </div>
            <div class="col-lg-2 col-md-4 col-12 text-center">
                <a href="https://m.facebook.com/UprotecJob/" target="_blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>&nbsp;
                <a href="https://twitter.com/protecjobs" target="_blank"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>&nbsp;
                <a href="https://www.instagram.com/uprotecjobs/" target="_blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>&nbsp;
                <a href="https://www.linkedin.com/company/uprotecjobs/" target="_blank"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a>&nbsp;
                <a href="https://www.youtube.com/channel/UCt0U3wkpzPjZmhwBhgiV3ew?view_as=subscriber" target="_blank"><i class="fa fa-youtube-square fa-2x" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="row align-items-center menu-bg1">
            <div class="col-lg-2 col-md-12 col-12"> <a href="{{url('/')}}" class="logo"><img src="{{ asset('/') }}sitesetting_images/thumb/{{ $siteSetting->site_logo }}" alt="{{ $siteSetting->site_name }}"/></a>
                <div class="navbar-header navbar-light">
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav-main" aria-controls="nav-main" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-10 col-md-12 col-12"> 
                <!-- Nav start -->
                <nav class="navbar navbar-expand-lg navbar-light">
					
                    <div class="navbar-collapse collapse" id="nav-main">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item {{ Request::url() == route('index') ? 'active' : '' }}"><a href="{{url('/')}}" class="nav-link">{{__('Home')}}</a> </li>
							
                            
							@if(Auth::guard('company')->check())
							<li class="nav-item"><a href="{{url('/job-seekers')}}" class="nav-link">{{__('Seekers')}}</a> </li>
							@else
							<li class="nav-item"><a href="{{url('/jobs')}}" class="nav-link">{{__('Jobs')}}</a> </li>
							@endif

							<li class="nav-item {{ Request::url()}}"><a href="{{url('/companies')}}" class="nav-link">{{__('Companies')}}</a> </li>
                            @foreach($show_in_top_menu as $top_menu) @php $cmsContent = App\CmsContent::getContentBySlug($top_menu->page_slug); @endphp
                            <li class="nav-item {{ Request::url() == route('cms', $top_menu->page_slug) ? 'active' : '' }}"><a href="{{ route('cms', $top_menu->page_slug) }}" class="nav-link">{{ $cmsContent->page_title }}</a> </li>
                            @endforeach
							<li class="nav-item {{ Request::url() == route('blogs') ? 'active' : '' }}"><a href="{{ route('blogs') }}" class="nav-link">{{__('Blog')}}</a> </li>
                            <li class="nav-item {{ Request::url() == route('contact.us') ? 'active' : '' }}"><a href="{{ route('contact.us') }}" class="nav-link">{{__('Contact us')}}</a> </li>
                            @if(Auth::check())
                            <li class="nav-item dropdown userbtn"><a href="">{{Auth::user()->printUserImage()}}</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a href="{{route('home')}}" class="nav-link"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a> </li>
                                    <li class="nav-item"><a href="{{ route('my.profile') }}" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> {{__('My Profile')}}</a> </li>
                                    <li class="nav-item"><a href="{{ route('view.public.profile', Auth::user()->id) }}" class="nav-link"><i class="fa fa-eye" aria-hidden="true"></i> {{__('View Public Profile')}}</a> </li>
                                    <li><a href="{{ route('my.job.applications') }}" class="nav-link"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('My Job Applications')}}</a> </li>
                                    <li class="nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();" class="nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a> </li>
                                    <form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </ul>
                            </li>
                            @endif @if(Auth::guard('company')->check())
                            <li class="nav-item postjob"><a href="{{route('post.job')}}" class="nav-link register">{{__('Post a job')}}</a> </li>
                            <li class="nav-item dropdown userbtn"><a href="">{{Auth::guard('company')->user()->printCompanyImage()}}</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a href="{{route('company.home')}}" class="nav-link"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a> </li>
                                    <li class="nav-item"><a href="{{ route('company.profile') }}" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> {{__('Company Profile')}}</a></li>
                                    <li class="nav-item"><a href="{{ route('post.job') }}" class="nav-link"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('Post Job')}}</a></li>
                                    <li class="nav-item"><a href="{{route('company.messages')}}" class="nav-link"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{__('Company Messages')}}</a></li>
                                    <li class="nav-item"><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();" class="nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a> </li>
                                    <form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </ul>
                            </li>
                            @endif @if(!Auth::user() && !Auth::guard('company')->user())
                            <li class="nav-item"><a href="{{route('login')}}" class="nav-link">{{__('Sign in')}}</a> </li>
							<li class="nav-item"><a href="{{route('register')}}" class="nav-link register">{{__('Register')}}</a> </li>                            
                            @endif
                            <!--<li class="dropdown userbtn"><a href="{{url('/')}}"><img src="{{asset('/')}}images/lang.png" alt="" class="userimg" /></a>
                                <ul class="dropdown-menu">
                                    @foreach($siteLanguages as $siteLang)
                                    <li><a href="javascript:;" onclick="event.preventDefault(); document.getElementById('locale-form-{{$siteLang->iso_code}}').submit();" class="nav-link">{{$siteLang->native}}</a>
                                        <form id="locale-form-{{$siteLang->iso_code}}" action="{{ route('set.locale') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="locale" value="{{$siteLang->iso_code}}"/>
                                            <input type="hidden" name="return_url" value="{{url()->full()}}"/>
                                            <input type="hidden" name="is_rtl" value="{{$siteLang->is_rtl}}"/>
                                        </form>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>-->
                        </ul>

                        <!-- Nav collapes end --> 

                    </div>
                    <div class="clearfix"></div>
                </nav>

                <!-- Nav end --> 

            </div>
        </div>

        <!-- row end --> 

    </div>

    <!-- Header container end --> 

</div>






<?php /*?>@if(!Auth::user() && !Auth::guard('company')->user())
	<div class="">my dive 2</div>
@endif<?php */?>