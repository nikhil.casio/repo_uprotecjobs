<?php $__env->startSection('content'); ?> 

<!-- Header start --> 

<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<!-- Header end --> 

<!-- Inner Page Title start --> 

<?php echo $__env->make('includes.inner_page_title', ['page_title'=>__('Register')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 


<!-- Inner Page Title end -->

<div class="listpgWraper">

    <div class="container">

        <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        

           <div class="useraccountwrap">

                <div class="userccount">

                    <div class="userbtns">

                        <ul class="nav nav-tabs">

                            <?php

                            $c_or_e = old('candidate_or_employer', 'candidate');

                            ?>

                            <li class="nav-item"><a class="nav-link <?php echo e(($c_or_e == 'candidate')? 'active':''); ?>" data-toggle="tab" href="#candidate" aria-expanded="true"><?php echo e(__('Candidate')); ?></a></li>

                            <li class="nav-item"><a class="nav-link <?php echo e(($c_or_e == 'employer')? 'active':''); ?>" data-toggle="tab" href="#employer" aria-expanded="false"><?php echo e(__('Employer')); ?></a></li>

                        </ul>

                    </div>

                    <div class="tab-content">

                        <div id="candidate" class="formpanel tab-pane <?php echo e(($c_or_e == 'candidate')? 'active':''); ?>">

                            <form class="form-horizontal" method="POST" action="<?php echo e(route('register')); ?>" data-parsley-validate>

                                <?php echo e(csrf_field()); ?>


                                <input type="hidden" name="candidate_or_employer" value="candidate" />

                                <div class="formrow<?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">

                                    <input type="text" name="first_name" class="form-control isAlpha" required  data-parsley-required-message="Please enter first name" placeholder="<?php echo e(__('First Name*')); ?>" value="<?php echo e(old('first_name')); ?>">

                                    <?php if($errors->has('first_name')): ?> <span class="help-block"> <strong><?php echo e($errors->first('first_name')); ?></strong> </span> <?php endif; ?> </div>

                                <div class="formrow<?php echo e($errors->has('middle_name') ? ' has-error' : ''); ?>">

                                    <input type="text" name="middle_name" class="form-control isAlpha" placeholder="<?php echo e(__('Middle Name')); ?>" value="<?php echo e(old('middle_name')); ?>"  data-parsley-required-message="Please enter middle name" >

                                    <?php if($errors->has('middle_name')): ?> <span class="help-block"> <strong><?php echo e($errors->first('middle_name')); ?></strong> </span> <?php endif; ?> </div>

                                <div class="formrow<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">

                                    <input type="text" name="last_name" class="form-control isAlpha" required="required" placeholder="<?php echo e(__('Last Name*')); ?>" value="<?php echo e(old('last_name')); ?>" required  data-parsley-required-message="Please enter last name">

                                    <?php if($errors->has('last_name')): ?> <span class="help-block"> <strong><?php echo e($errors->first('last_name')); ?></strong> </span> <?php endif; ?> </div>
                                    
                                

                                <div class="formrow<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">

                                    <input type="email" name="email" class="form-control" required="required" placeholder="<?php echo e(__('Email*')); ?>" value="<?php echo e(old('email')); ?>"  required
                                                   data-parsley-required-message="Please enter email id"
                                                   data-parsley-pattern="/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}/g"
                                                   data-parsley-pattern-message="Invalid email address."
                                                   data-parsley-type="email"
                                                   data-parsley-type-message="Please enter valid email id">

                                    <?php if($errors->has('email')): ?> <span class="help-block"> <strong><?php echo e($errors->first('email')); ?></strong> </span> <?php endif; ?> </div>
                                
                                <div class="formrow<?php echo e($errors->has('mobile_num') ? ' has-error' : ''); ?>">

                                    <input type="text" name="mobile_num" class="form-control isInteger mobile_num" required="required" placeholder="<?php echo e(__('Mobile Number*')); ?>" maxlength="11"  value="<?php echo e(old('mobile_num')); ?>" required  data-parsley-required-message="Please enter mobile number">
                                    
                                    <?php if($errors->has('mobile_num')): ?> <span class="help-block"> <strong><?php echo e($errors->first('mobile_num')); ?></strong> </span> <?php endif; ?> </div>
                                    
                                <div class="formrow<?php echo e($errors->has('otp') ? ' has-error' : ''); ?>" hidden>

                                    <input type="text" name="otp" class="form-control otp isInteger"  maxlength="4" placeholder="<?php echo e(__('Enter OTP*')); ?>" maxlength="11"  value="<?php echo e(old('mobile_num')); ?>"   data-parsley-required-message="Please enter OTP">

                                    <?php if($errors->has('otp')): ?> <span class="help-block"> <strong><?php echo e($errors->first('otp')); ?></strong> </span> <?php endif; ?> <br>
                                    <span class="text-success txtmsg"></span>
                                    <a href="javascript:(0)" class="pull-right sendotp">Send Otp</a> <br>   
                                </div>

                                <div class="formrow<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">

                                    <input type="password" id="password" name="password" class="form-control" placeholder="<?php echo e(__('Password*')); ?>" value=""  data-parsley-required-message="Please enter password"
                                                   data-parsley-uppercase="1"
                                                   data-parsley-lowercase="1"
                                                   data-parsley-number="1"
                                                   data-parsley-special="1" data-parsley-required autocomplete="false" onKeyUp="checkCandidatePasswordStrength();" >
                                                   
                                                   <div id="candidate-password-strength-status"></div>

                                    <?php if($errors->has('password')): ?> <span class="help-block"> <strong><?php echo e($errors->first('password')); ?></strong> </span> <?php endif; ?> </div>

                                <div class="formrow<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">

                                    <input type="password" name="password_confirmation" class="form-control"  placeholder="Enter confirm password" id="confirm_password"
                                                   data-parsley-required-message="Please enter confirm password"
                                                   data-parsley-equalto="#password"
                                                   data-parsley-equalto-message="Confirm password and password must be same."
                                                   data-parsley-required placeholder="<?php echo e(__('Password Confirmation*')); ?>" value="">

                                    <?php if($errors->has('password_confirmation')): ?> <span class="help-block"> <strong><?php echo e($errors->first('password_confirmation')); ?></strong> </span> <?php endif; ?> </div>
                                
                                

                                    

                                    <div class="formrow<?php echo e($errors->has('is_subscribed') ? ' has-error' : ''); ?>">

    <?php

	$is_checked = '';

	if (old('is_subscribed', 1)) {

		$is_checked = 'checked="checked"';

	}

	?>

                                    

                                    <input type="checkbox" value="1" name="is_subscribed" <?php echo e($is_checked); ?> /><?php echo e(__('Subscribe to news letter')); ?>


                                    <?php if($errors->has('is_subscribed')): ?> <span class="help-block"> <strong><?php echo e($errors->first('is_subscribed')); ?></strong> </span> <?php endif; ?> </div>

                                    

                                    

                                <div class="formrow<?php echo e($errors->has('terms_of_use') ? ' has-error' : ''); ?>">

                                    <input type="checkbox" value="1" name="terms_of_use" />

                                    <a href="<?php echo e(url('cms/terms-of-use')); ?>"><?php echo e(__('I accept Terms of Use')); ?></a>



                                    <?php if($errors->has('terms_of_use')): ?> <span class="help-block"> <strong><?php echo e($errors->first('terms_of_use')); ?></strong> </span> <?php endif; ?> </div>

                             

                                <input type="submit" class="btn" value="<?php echo e(__('Register')); ?>">

                            </form>

                        </div>

                        <div id="employer" class="formpanel tab-pane fade <?php echo e(($c_or_e == 'employer')? 'active':''); ?>">

                            <form class="form-horizontal" method="POST" action="<?php echo e(route('company.register')); ?>" data-parsley-validate>

                                <?php echo e(csrf_field()); ?>


                                <input type="hidden" name="candidate_or_employer" value="employer" />

                                <div class="formrow<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">

                                    <input type="text" name="name" class="form-control" required="required" placeholder="<?php echo e(__('Company Name*')); ?>" value="<?php echo e(old('name')); ?>" required  data-parsley-required-message="Please enter name" >

                                    <?php if($errors->has('name')): ?> <span class="help-block"> <strong><?php echo e($errors->first('name')); ?></strong> </span> <?php endif; ?> </div>
                                    
                                <!--<div class="formrow<?php echo e($errors->has('company_name') ? ' has-error' : ''); ?>">

                                    <input type="text" name="company_name" class="form-control" required="required" placeholder="<?php echo e(__('Company Name*')); ?>" value="<?php echo e(old('company_name')); ?>" required  data-parsley-required-message="Please enter company name" >

                                    <?php if($errors->has('company_name')): ?> <span class="help-block"> <strong><?php echo e($errors->first('company_name')); ?></strong> </span> <?php endif; ?> </div>-->
                                    
                                <div class="formrow<?php echo e($errors->has('gst_no') ? ' has-error' : ''); ?>">

                                    <!--<input type="text" name="gst_no" class="form-control"  placeholder="<?php echo e(__('GST Number')); ?>" value="<?php echo e(old('gst_no')); ?>"   
                                    data-parsley-required-message="Please enter GST number">-->
                                    <input type="text" name="gst_no" class="form-control"  placeholder="<?php echo e(__('GST Number')); ?>" value="<?php echo e(old('gst_no')); ?>"   
                                    data-parsley-required-message="Please enter GST number" 
                                    data-parsley-pattern="/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/g" 
                                                   data-parsley-pattern-message="Please Enter Valid GSTIN Number">

                                    <?php if($errors->has('gst_no')): ?> <span class="help-block"> <strong><?php echo e($errors->first('gst_no')); ?></strong> </span> <?php endif; ?> </div>
                                    
                                <div class="formrow<?php echo e($errors->has('mobile_num') ? ' has-error' : ''); ?>">

                                    <input type="text" name="mobile_num" class="form-control emobile_num isInteger " required="required" placeholder="<?php echo e(__('Mobile Number*')); ?>" maxlength="11" value="<?php echo e(old('mobile_num')); ?>" required  data-parsley-required-message="Please enter mobile number" >

                                    <?php if($errors->has('mobile_num')): ?> <span class="help-block"> <strong><?php echo e($errors->first('mobile_num')); ?></strong> </span> <?php endif; ?> </div>
                                    
                                    <div class="formrow<?php echo e($errors->has('otp') ? ' has-error' : ''); ?>" hidden>

                                    <input type="text" name="otp" class="form-control  eotp isInteger"  maxlength="4" placeholder="<?php echo e(__('Enter OTP*')); ?>" maxlength="11"  value="<?php echo e(old('mobile_num')); ?>"   data-parsley-required-message="Please enter OTP">

                                    <?php if($errors->has('otp')): ?> <span class="help-block"> <strong><?php echo e($errors->first('otp')); ?></strong> </span> <?php endif; ?> <br>
                                    <span class="text-success etxtmsg"></span>
                                    <a href="javascript:(0)" class="pull-right esendotp">Send Otp</a> <br>   
                                </div>

                                <div class="formrow<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">

                                    <input type="email" name="email" class="form-control"  placeholder="<?php echo e(__('Email*')); ?>" value="<?php echo e(old('email')); ?>" required
                                                   data-parsley-required-message="Please enter email id"
                                                   data-parsley-pattern="/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}/g"
                                                   data-parsley-pattern-message="Invalid email address."
                                                   data-parsley-type="email"
                                                   data-parsley-type-message="Please enter valid email id">

                                    <?php if($errors->has('email')): ?> <span class="help-block"> <strong><?php echo e($errors->first('email')); ?></strong> </span> <?php endif; ?> </div>

                                <div class="formrow<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">

                                    <input type="password" name="password" class="form-control" id="password_employee" placeholder="Enter password" data-parsley-minlength="8"
                                                   data-parsley-required-message="Please enter password"
                                                   data-parsley-uppercase="1"
                                                   data-parsley-lowercase="1"
                                                   data-parsley-number="1"
                                                   data-parsley-special="1" data-parsley-required autocomplete="false" placeholder="<?php echo e(__('Password*')); ?>" value="" onKeyUp="checkEmployPasswordStrength();">
                                         <div id="employee_password_length_status"></div>
                                    <?php if($errors->has('password')): ?> <span class="help-block"> <strong><?php echo e($errors->first('password')); ?></strong> </span> <?php endif; ?> </div>

                                <div class="formrow<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">

                                    <input type="password" name="password_confirmation" class="form-control" data-parsley-required-message="Please enter confirm password"
                                                   data-parsley-equalto="#password_employee"
                                                   data-parsley-equalto-message="Confirm password and password must be same."
                                                   data-parsley-required placeholder="<?php echo e(__('Password Confirmation*')); ?>" value="">

                                    <?php if($errors->has('password_confirmation')): ?> <span class="help-block"> <strong><?php echo e($errors->first('password_confirmation')); ?></strong> </span> <?php endif; ?> </div>

                                    <div class="formrow<?php echo e($errors->has('is_subscribed') ? ' has-error' : ''); ?>">

    <?php

	$is_checked = '';

	if (old('is_subscribed', 1)) {

		$is_checked = 'checked="checked"';

	}

	?>

                                    

                                    <input type="checkbox" value="1" name="is_subscribed" <?php echo e($is_checked); ?> /><?php echo e(__('Subscribe to news letter')); ?>


                                    <?php if($errors->has('is_subscribed')): ?> <span class="help-block"> <strong><?php echo e($errors->first('is_subscribed')); ?></strong> </span> <?php endif; ?> </div>

                                <div class="formrow<?php echo e($errors->has('terms_of_use') ? ' has-error' : ''); ?>">

                                    <input type="checkbox" value="1" name="terms_of_use" />

                                    <a href="<?php echo e(url('terms-of-use')); ?>"><?php echo e(__('I accept Terms of Use')); ?></a>



                                    <?php if($errors->has('terms_of_use')): ?> <span class="help-block"> <strong><?php echo e($errors->first('terms_of_use')); ?></strong> </span> <?php endif; ?> </div>

                            

                                <input type="submit" class="btn" value="<?php echo e(__('Register')); ?>">

                            </form>

                        </div>

                    </div>

                    <!-- sign up form -->

                    <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> <?php echo e(__('Have Account')); ?>? <a href="<?php echo e(route('login')); ?>"><?php echo e(__('Sign in')); ?></a></div>

                    <!-- sign up form end--> 



                </div>

            </div>

        

    </div>

</div>

 <script>
        function checkCandidatePasswordStrength() {
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 6) {
                $('#candidate-password-strength-status').removeClass();
                $('#candidate-password-strength-status').addClass('weak-password');
                $('#candidate-password-strength-status').html("Weak (should be atleast 6 characters.)");
            } else {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                    $('#candidate-password-strength-status').removeClass();
                    $('#candidate-password-strength-status').addClass('strong-password');
                    $('#candidate-password-strength-status').html("Strong");
                } else {
                    $('#candidate-password-strength-status').removeClass();
                    $('#candidate-password-strength-status').addClass('medium-password');
                    $('#candidate-password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }
        
         function checkEmployPasswordStrength() {
            var number = /([0-9])/;m
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password_employee').val().length < 6) {
                $('#employee_password_length_Status').removeClass();
                $('#employee_password_length_Status').addClass('weak-password');
                $('#employee_password_length_Status').html("Weak (should be atleast 6 characters.)");
            } else {
                if ($('#password_employ').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                    $('#employee_password_length_Status').removeClass();
                    $('#employee_password_length_Status').addClass('strong-password');
                    $('#employee_password_length_Status').html("Strong");
                } else {
                    $('#employee_password_length_Status').removeClass();
                    $('#employee_password_length_Status').addClass('medium-password');
                    $('#employee_password_length_Status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }
    </script>



<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?> 

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>