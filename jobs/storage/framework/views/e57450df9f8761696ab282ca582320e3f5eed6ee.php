<?php
$lang = config('default_lang');
if (isset($testimonial))
    $lang = $testimonial->lang;
$lang = MiscHelper::getLang($lang);
$direction = MiscHelper::getLangDirection($lang);
$queryString = MiscHelper::getLangQueryStr();
?>
<?php echo APFrmErrHelp::showErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">        
    <?php echo Form::hidden('id', null); ?>

    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'lang'); ?>" id="lang_div">
        <?php echo Form::select('lang', ['' => 'Select Language']+$languages, $lang, ['class'=>'form-control', 'id'=>'lang', 'onchange'=>'setLang(this.value);']); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'lang'); ?>                                       
    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'testimonial_by'); ?>">
        <?php echo Form::label('testimonial_by', 'Testimonial By', ['class' => 'bold']); ?>

        <?php echo Form::text('testimonial_by', null, array('class'=>'form-control', 'id'=>'testimonial_by', 'placeholder'=>'Testimonial By', 'dir'=>$direction)); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'testimonial_by'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'testimonial'); ?>">
        <?php echo Form::label('testimonial', 'Testimonial', ['class' => 'bold']); ?>

        <?php echo Form::textarea('testimonial', null, array('class'=>'form-control', 'id'=>'testimonial', 'placeholder'=>'Testimonial', 'dir'=>$direction)); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'testimonial'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'company'); ?>">
        <?php echo Form::label('company', 'Company and Designation', ['class' => 'bold']); ?>

        <?php echo Form::text('company', null, array('class'=>'form-control', 'id'=>'company', 'placeholder'=>'Company and Designation', 'dir'=>$direction)); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'company'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'is_default'); ?>">
        <?php echo Form::label('is_default', 'Is default?', ['class' => 'bold']); ?>

        <div class="radio-list">
            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($testimonial)) ? $testimonial->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="default" name="is_default" type="radio" value="1" <?php echo e($is_default_1); ?> onchange="showHideTestimonialId();">
                Yes </label>
            <label class="radio-inline">
                <input id="not_default" name="is_default" type="radio" value="0" <?php echo e($is_default_2); ?> onchange="showHideTestimonialId();">
                No </label>
        </div>			
        <?php echo APFrmErrHelp::showErrors($errors, 'is_default'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'testimonial_id'); ?>" id="testimonial_id_div">
        <?php echo Form::label('testimonial_id', 'Default Testimonial', ['class' => 'bold']); ?>                    
        <?php echo Form::select('testimonial_id', ['' => 'Select Default Testimonial']+$testimonials, null, array('class'=>'form-control', 'id'=>'testimonial_id')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'testimonial_id'); ?>                                       
    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'is_active'); ?>">
        <?php echo Form::label('is_active', 'Is Active?', ['class' => 'bold']); ?>

        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($testimonial)) ? $testimonial->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" <?php echo e($is_active_1); ?>>
                Active </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" <?php echo e($is_active_2); ?>>
                In-Active </label>
        </div>			
        <?php echo APFrmErrHelp::showErrors($errors, 'is_active'); ?>

    </div>	
    <div class="form-actions">
        <?php echo Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')); ?>

    </div>
</div>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
    function setLang(lang) {
        window.location.href = "<?php echo url(Request::url()) . $queryString; ?>" + lang;
    }
    function showHideTestimonialId() {
        $('#testimonial_id_div').hide();
        var is_default = $("input[name='is_default']:checked").val();
        if (is_default == 0) {
            $('#testimonial_id_div').show();
        }
    }
    showHideTestimonialId();
</script>
<?php $__env->stopPush(); ?>