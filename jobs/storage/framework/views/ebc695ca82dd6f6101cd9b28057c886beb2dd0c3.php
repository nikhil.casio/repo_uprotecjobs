<?php $__env->startSection('content'); ?> 
<!-- Header start --> 
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- Header end --> 
<!-- Inner Page Title start --> 
<?php echo $__env->make('includes.inner_page_title', ['page_title'=>__('Pay with Stripe')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row"> 
            <?php if(Auth::guard('company')->check()): ?>
            <?php echo $__env->make('includes.company_dashboard_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php else: ?>
            <?php echo $__env->make('includes.user_dashboard_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
            <div class="col-md-9 col-sm-8">
                <div class="userccount">
                    <div class="row">
                        <div class="col-md-5">
                            <img src="<?php echo e(asset('/')); ?>images/strip-logo.png" alt="" />
                            <div class="strippckinfo">
                                <h5><?php echo e(__('Invoice Details')); ?></h5>
                                <div class="pkginfo"><?php echo e(__('Package')); ?>: <strong><?php echo e($package->package_title); ?></strong></div>
                                <div class="pkginfo"><?php echo e(__('Price')); ?>: <strong>$<?php echo e($package->package_price); ?></strong></div>

                                <?php if(Auth::guard('company')->check()): ?>
                                <div class="pkginfo"><?php echo e(__('Can post jobs')); ?>: <strong><?php echo e($package->package_num_listings); ?></strong></div>
                                <?php else: ?>
                                <div class="pkginfo"><?php echo e(__('Can apply on jobs')); ?>: <strong><?php echo e($package->package_num_listings); ?></strong></div>
                                <?php endif; ?>
                                <div class="pkginfo"><?php echo e(__('Package Duration')); ?>: <strong><?php echo e($package->package_num_days); ?> <?php echo e(__('Days')); ?></strong></div>
                            </div>




                        </div>
                        <div class="col-md-7">
                            <div class="formpanel"> <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <h5><?php echo e(__('Strip - Credit Card Details')); ?></h5>
                                <?php                
                                $route = 'stripe.order.upgrade.package';                
                                if($new_or_upgrade == 'new'){                
                                $route = 'stripe.order.package';                
                                }                
                                ?>                            
                                <?php echo Form::open(array('method' => 'post', 'route' => $route, 'id' => 'stripe-form', 'class' => 'form')); ?>                
                                <?php echo e(Form::hidden('package_id', $package_id)); ?>

                                <div class="row">
                                    <div class="col-md-12" id="error_div"></div>
                                    <div class="col-md-12">
                                        <div class="formrow">
                                            <label><?php echo e(__('Name on Credit Card')); ?></label>
                                            <input class="form-control" id="card_name" placeholder="<?php echo e(__('Name on Credit Card')); ?>" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="formrow">
                                            <label><?php echo e(__('Credit card Number')); ?></label>
                                            <input class="form-control" id="card_no" placeholder="<?php echo e(__('Credit card Number')); ?>" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formrow">
                                            <label><?php echo e(__('Credit card Expiry Month')); ?></label>                     
                                            <select class="form-control" id="ccExpiryMonth">                    
                                                <?php for($counter = 1; $counter <= 12; $counter++): ?>
                                                <?php
                                                $val = str_pad($counter, 2, '0', STR_PAD_LEFT);
                                                ?>
                                                <option value="<?php echo e($val); ?>"><?php echo e($val); ?></option>
                                                <?php endfor; ?>
                                            </select>                    
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formrow">
                                            <label><?php echo e(__('Credit card Expiry Year')); ?></label>                    
                                            <select class="form-control" id="ccExpiryYear">
                                                <?php
                                                $ccYears = MiscHelper::getCcExpiryYears();
                                                ?>
                                                <?php $__currentLoopData = $ccYears; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($year); ?>"><?php echo e($year); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>                    
                                        </div>
                                    </div>                  
                                    <div class="col-md-12">
                                        <div class="formrow">
                                            <label><?php echo e(__('CVV Number')); ?></label>
                                            <input class="form-control" id="cvvNumber" placeholder="<?php echo e(__('CVV number')); ?>" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="formrow">
                                            <button type="submit" class="btn"><?php echo e(__('Pay with Stripe')); ?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('styles'); ?>
<style type="text/css">
    .userccount p{ text-align:left !important;}
</style>
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?> 
<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 
<script type="text/javascript">
Stripe.setPublishableKey('<?php echo e(Config::get('stripe.stripe_key')); ?>');
var $form = $('#stripe-form');
$form.submit(function (event) {
    $('#error_div').hide();
    $form.find('button').prop('disabled', true);
    Stripe.card.createToken({
        number: $('#card_no').val(),
        cvc: $('#cvvNumber').val(),
        exp_month: $('#ccExpiryMonth').val(),
        exp_year: $('#ccExpiryYear').val(),
        name: $('#card_name').val()
    }, stripeResponseHandler);
    return false;
});
function stripeResponseHandler(status, response) {
    if (response.error) {
        $('#error_div').show();
        $('#error_div').text(response.error.message);
        $form.find('button').prop('disabled', false);
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        // Submit the form:
        $form.get(0).submit();
    }
}
</script> 
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>