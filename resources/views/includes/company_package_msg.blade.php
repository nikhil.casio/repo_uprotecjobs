<div class="instoretxt pack-msg">
    <div class="row">
        <div class="credit col-md-6 col-sm-6 col xs-12">
            {{__('Your Package is')}}:
            <strong>{{$package->package_title}} {{__('-')}} {{ $siteSetting->default_currency_code }} {{$package->package_price}}</strong>
        </div>
        <div class="credit col-md-6 col-sm-6 col xs-12">
            {{__('Package Duration')}} : 
            <strong>{{Auth::guard('company')->user()->package_start_date->format('d M, Y')}} {{__('-')}} {{Auth::guard('company')->user()->package_end_date->format('d M, Y')}}</strong>
        </div>
        <div class="credit col-md-6 col-sm-6 col xs-12">
            {{__('Availed quota')}} :
            <strong>{{Auth::guard('company')->user()->availed_jobs_quota}} {{__('/')}} {{Auth::guard('company')->user()->jobs_quota}}</strong></div>
        <div class="credit col-md-6 col-sm-6 col xs-12">
            {{__('Availed Active Post')}} :
            <strong>{{Auth::guard('company')->user()->availed_required_active_post}} {{__('/')}} {{Auth::guard('company')->user()->total_required_active_post}}</strong></div>
    </div>
</div>
