<div class="paypackages"> 
    <!---four-paln-->
    <div class="four-plan">
        <h3>{{__('Our Packages')}}</h3>
        <div class="row"> @foreach($packages as $package)
            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="boxes">
                    <li class="plan-name">{{$package->package_title}}</li>
                    <li>
                        <div class="main-plan text-center">
                            <!--<div class="plan-price1-1">{{ $siteSetting->default_currency_code }}</div>-->
                            <div class="plan-price1-1">INR <span class="plan-price1-2">{{$package->package_price}}</span></div>
                              <div class="gst-rates">+18%GST</div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <li class="plan-pages">{{__('Can post jobs')}} : {{$package->package_num_listings}}</li>
                    <li class="plan-pages">{{__('Package Duration')}} : {{$package->package_num_days}} {{__('Days')}}</li>                    
                    <li class="plan-pages">{{$package->unlocking}}</li>                    
                    <li class="plan-pages">{{$package->unlimited}}</li>            
                     @if($package->package_price == 5000)
                     <li class="plan-pages">Active Post : Unlimited</li>
                     @else
                        <li class="plan-pages">Active Post : {{$package->total_active_post}}</li>
                     @endif
                     
                      @if($package->package_price == 0)
                    <li class="plan-pages">No Of Cities Selection : 1</li>
                    <li class="plan-pages">Best Of Hiring : Up to 3</li>
                    <li class="plan-pages">Candidate Database Unlocks : 100</li>
                    <li class="plan-pages">Priority Customer Support : NA</li>
                    <li class="plan-pages">Listing In Featured Jobs : NA</li>
                    <li class="plan-pages">Personalised Notification Of Your Job To Candidates : NA</li>
                    @endif
                    
                     @if($package->package_price == 2000)
                    <li class="plan-pages">No Of Cities Selection : Unlimited</li>
                    <li class="plan-pages">Best Of Hiring : Up to 5</li>
                    <li class="plan-pages">Candidate Database Unlocks : Unlimited</li>
                    <li class="plan-pages">Priority Customer Support : NA</li>
                    <li class="plan-pages">Listing In Featured Jobs : NA</li>
                    <li class="plan-pages">Personalised Notification Of Your Job To Candidates : NA</li>
                    @endif
                    
                     @if($package->package_price == 5000)
                    <li class="plan-pages">No Of Cities Selection : Unlimited</li>
                    <li class="plan-pages">Best Of Hiring : Up to 10</li>
                    <li class="plan-pages">Candidate Database Unlocks : Unlimited</li>
                    <li class="plan-pages">Priority Customer Support : NA</li>
                    <li class="plan-pages">Listing In Featured Jobs : NA</li>
                    <li class="plan-pages">Personalised Notification Of Your Job To Candidates : Yes</li>
                    @endif
    
                     
                    @if($package->package_price > 0)                        
                        @if((bool)$siteSetting->is_paypal_active)
                        <li class="order paypal"><a href="{{route('order.package', $package->id)}}"><i class="fa fa-cc-paypal" aria-hidden="true"></i> {{__('pay with paypal')}}</a></li>
                        @endif
                        @if((bool)$siteSetting->is_stripe_active)
                        <li class="order"><a href="{{route('stripe.order.form', [$package->id, 'new'])}}"><i class="fa fa-cc-stripe" aria-hidden="true"></i> {{__('pay with stripe')}}</a></li>
                        @endif
                        <li class="order"><a href="{{route('payu.order.form', [$package->id, 'upgrade'])}}"><i class="fa fa-money" aria-hidden="true"></i> {{__('pay with Payumoney')}}</a></li>
                        
                    @else
                    <li class="order paypal"><a href="{{route('order.free.package', $package->id)}}"> {{__('Subscribe Free Package')}}</a></li>
                    @endif
                </ul>
            </div>
            @endforeach </div>
    </div>
    <!---end four-paln--> 
</div>
