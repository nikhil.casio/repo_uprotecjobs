@if(!Auth::user() && !Auth::guard('company')->user())
<div class="emploginbox">
	<div class="container">		
		<div class="titleTop">
			<div class="subtitle">{{__('Are You Looking For Candidates!')}}</div>
           <h3>{{__('Post a Job Today')}}  </h3>
			<h4>{{__('“Hire right, because the penalties of hiring wrong are huge.” — Ray Dalio')}}</h4>
        </div>
		<p>Are you struggling to find the right person for a job? Are your strategies failing in the execution mode? Don't compromise with an amateur, hire an expert.</p>
		<div class="viewallbtn"><a href="{{route('register')}}">{{__('Post a Job')}}</a></div>
	</div>
</div>
@endif