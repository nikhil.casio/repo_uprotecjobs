<!--Footer-->
<div class="largebanner shadow3">
    <div class="adin">
        {!! $siteSetting->above_footer_ad !!}
    </div>
    <div class="clearfix"></div>
</div>


<div class="footerWrap">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-6">
                <div class="row">
                    <!--Quick Links-->
                    <div class="col-md-3 col-sm-6">
                        <h5>
                            {{__('Job Seekers')}}
                            <span class="d-md-none footer-quicks" data-target="footer-job-seekers"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Quick Links menu Start-->
                        <ul class="quicklinks" id="footer-job-seekers">
                            <li><a href="{{ route('job.list') }}">{{__('Search Job')}}</a></li>
                            <li><a href="{{ route('blogs') }}">{{__('Career Advice')}}</a></li>
                            <li><a href="{{ route('job.list') }}?job_experience_id%5B%5D=11">{{__('Jobs for Freshers')}}</a></li>
                            <li><a href="{{ route('job.list') }}?job_experience_id%5B%5D=5">{{__('Jobs for Experienced')}}</a></li>
                        </ul>
                    </div>
                    <!--Quick Links menu end-->
                    <!--Quick Links-->
                    <div class="col-md-3 col-sm-6">
                        <h5>
                            {{__('For Recruiters')}}
                            <span class="d-md-none footer-quicks" data-target="footer-for-recruiters"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Quick Links menu Start-->
                        <ul class="quicklinks" id="footer-for-recruiters">
                            <li><a href="{{ route('post.job') }}">{{__('Post a Job')}}</a></li>
                            <li><a href="{{ route('register') }}">{{__('Search Candidates')}}</a></li>
                            <li><a href="{{ route('index') }}">{{__('Hiring Plans')}}</a></li>
                            <li><a href="{{ route('index') }}">{{__('Resume Database Access')}}</a></li>
                        </ul>
                    </div>
                    <!--Quick Links menu end-->

                    <div class="col-md-3 col-sm-6">
                        <h5>
                            {{__('Jobs By Functional Area')}}
                            <span class="d-md-none footer-quicks" data-target="footer-funct-area"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Quick Links menu Start-->
                        <ul class="quicklinks" id="footer-funct-area">
                            @php
                            $functionalAreas = App\FunctionalArea::getUsingFunctionalAreas(10);
                            @endphp
                            @foreach($functionalAreas as $functionalArea)
                            <li><a
                                    href="{{ route('job.list', ['functional_area_id[]'=>$functionalArea->functional_area_id]) }}">{{$functionalArea->functional_area}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <!--Jobs By Industry-->
                    <div class="col-md-3 col-sm-6">
                        <h5>
                            {{__('Jobs By Industry')}}
                            <span class="d-md-none footer-quicks" data-target="footer-by-industry"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Industry menu Start-->
                        <ul class="quicklinks" id="footer-by-industry">
                            @php
                            $industries = App\Industry::getUsingIndustries(10);
                            @endphp
                            @foreach($industries as $industry)
                            <li><a
                                    href="{{ route('job.list', ['industry_id[]'=>$industry->industry_id]) }}">{{$industry->industry}}</a>
                            </li>
                            @endforeach
                        </ul>
                        <!--Industry menu End-->
                        <div class="clear"></div>
                    </div>
                        <div class="clear"></div>
                    </div>
            </div>


            <!--About Us-->
            <div class="col-md-3 col-sm-12">
                <h5>{{__('Contact Us')}}</h5>
                <div class="address">{{ $siteSetting->site_street_address }}</div>
                <div class="email"> <a
                        href="mailto:{{ $siteSetting->mail_to_address }}">{{ $siteSetting->mail_to_address }}</a> </div>
                <div class="phone"> <a
                        href="tel:{{ $siteSetting->site_phone_primary }}" style="font-size:14px !important;">{{ $siteSetting->site_phone_primary }}</a>
                </div>
                <!-- Social Icons -->
                <div class="social">@include('includes.footer_social')</div>
                <!-- Social Icons end -->
            </div>
            <!--About us End-->


        </div>
    </div>
</div>
<!--Footer end-->
<!--Copyright-->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!--<div class="bttxt">{{__('Copyright')}} &copy; {{date('Y')}} {{ $siteSetting->site_name }}. {{__('All Rights Reserved')}}. {{__('Design by')}}: <a href="{{url('/')}}http://uprotecjobs.com/jobs" target="_blank">uprotecjob</a></div>-->
                <div class="bttxt">{{__('Copyright')}} &copy; {{date('Y')}} {{ $siteSetting->site_name }}.
                    {{__('All Rights Reserved')}}.</div>
            </div>
            <div class="col-md-4 text-right">
                @foreach($show_in_footer_menu as $footer_menu)
                @php
                $cmsContent = App\CmsContent::getContentBySlug($footer_menu->page_slug);
                @endphp
                <a class="{{ $footer_menu->page_slug }}" href="{{ route('cms', $footer_menu->page_slug) }}">{{ $cmsContent->page_title }}</a>&nbsp;&nbsp;&nbsp;
                
                @endforeach
                <div class="paylogos"><img src="{{asset('/')}}images/payment-icons.png" alt="" /></div>
            </div>
        </div>

    </div>
</div>
<?php //print_r($_COOKIE); echo $_COOKIE['laravel_session'];?>
<?php //if($_COOKIE['laravel_session']=='') {?>
<div class="cookieConsentContainer">
    <div class="cookieTitle">
        <h3>Cookies</h3>
    </div>
    <div class="cookieDesc">
        <p>This website uses cookies to ensure you get the best experience on our website

        </p>
    </div>
    <div class="cookieButton">
        <a href="javascript:void(0)" class="button btn btn-primary cookieAcceptButton">Accept</a>
    </div>
</div>
<?php //} ?>
<script>
    document.addEventListener("DOMContentLoaded",function(){
        $(".footer-quicks").on("click",function(){
            var target = $(this).data("target");
            $("#"+target).toggle();
        })
    })
</script>