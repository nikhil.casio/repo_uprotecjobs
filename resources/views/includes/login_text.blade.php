
<div class="userloginbox">
	<div class="container">		
		<div class="titleTop">
           <h3>{{__('Are You Looking For Job!')}} </h3>
        </div>
		<h3>Choose a job you love, and you will never have to work a day in your life." — Confucius</h3>
<p>Is your job still not the reason for getting up in the morning? Is something still missing? A global poll conducted by Gallup shows that 85% people are unhappy in their current job. You don't have to be one of them.
</p>
		
		@if(!Auth::user() && !Auth::guard('company')->user())
		<div class="viewallbtn"><a href="{{route('register')}}"> {{__('Get Started Today')}} </a></div>
		@else
		<div class="viewallbtn"><a href="{{url('my-profile')}}">{{__('Build Your CV')}} </a></div>
		@endif
	</div>
</div>
