<?php
if (!isset($seo)) {
    $seo = (object)array('seo_title' => $siteSetting->site_name, 'seo_description' => $siteSetting->site_name, 'seo_keywords' => $siteSetting->site_name, 'seo_other' => '');
}
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="{{ (session('localeDir', 'ltr'))}}" dir="{{ (session('localeDir', 'ltr'))}}">

<head>
    
    <!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '939571689891409');

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1"

src="https://www.facebook.com/tr?id=939571689891409&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{__($seo->seo_title) }}</title>
    <meta name="Description" content="{!! $seo->seo_description !!}">
    <meta name="Keywords" content="{!! $seo->seo_keywords !!}">
    {!! $seo->seo_other !!}
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="{{asset('/')}}favicon.ico">
    <!--<link rel="shortcut icon" href="{{asset('/')}}fevi-uprotec.png">-->
    <!-- Slider -->
    <link href="{{asset('/')}}js/revolution-slider/css/settings.css" rel="stylesheet">
    <!-- Owl carousel -->
    <link href="{{asset('/')}}css/owl.carousel.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('/')}}css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('/')}}css/font-awesome.css" rel="stylesheet">
    <!-- Custom Style -->
    <link href="{{asset('/')}}css/main.css" rel="stylesheet">
    @if((session('localeDir', 'ltr') == 'rtl'))
    <!-- Rtl Style -->
    <link href="{{asset('/')}}css/rtl-style.css" rel="stylesheet">
     <link href="{{asset('/')}}css/parsley.css" rel="stylesheet">
     
    @endif
    <link href="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="{{asset('/')}}js/html5shiv.min.js"></script>
          <script src="{{asset('/')}}js/respond.min.js"></script>
        <![endif]-->
    @stack('styles')
</head>

<body>
    @yield('content')
    <!-- Bootstrap's JavaScript -->
    <script src="{{asset('/')}}js/jquery.min.js"></script>
    <script src="{{asset('/')}}js/js_common_validations.js"></script>
    <script src="{{asset('/')}}js/bootstrap.min.js"></script>
    <script src="{{asset('/')}}js/popper.js"></script>
     <script src="{{asset('/')}}js/parsley.min.js"></script>
    <!-- Owl carousel -->
    <script src="{{asset('/')}}js/owl.carousel.js"></script>
    <script src="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{ asset('/') }}admin_assets/global/plugins/Bootstrap-3-Typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="{{ asset('/') }}admin_assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="{{ asset('/') }}admin_assets/global/plugins/jquery.scrollTo.min.js" type="text/javascript"></script>
    <!-- Revolution Slider -->
    <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
   
    {!! NoCaptcha::renderJs() !!}
    @stack('scripts')
    <!-- Custom js -->
    <script src="{{asset('/')}}js/script.js"></script>
    <script type="text/JavaScript">
        $(document).ready(function(){
            $(document).scrollTo('.has-error', 2000);
            });
            function showProcessingForm(btn_id){		
            $("#"+btn_id).val( 'Processing .....' );
            $("#"+btn_id).attr('disabled','disabled');		
            }
        </script>
        <script>
        $(document).ready(function(){
          if (localStorage.getItem('job-cookie') != '1') {
             $('.cookieConsentContainer').delay(2000).fadeIn();
             $('.cookieConsentContainer').css('display','block');
          }else{
              $('.cookieConsentContainer').css('display','none');
          }
          $('.cookieAcceptButton').on('click',function() {
              localStorage.setItem('job-cookie','1');
             $('.cookieConsentContainer').fadeOut();
             
          }); 
        });
    </script>
    
<script>
    $(document).ready(function(){
        
        $(".sendotp").click(function(){
            var mobile = $("#candidate .mobile_num").val();
            if(mobile=='')
            {
                alert('Please enter mobile number');
                return false;
            }else{
                otp(mobile);
                return false;
            }
        });
        
        $(".esendotp").click(function(){
            var mobile = $("#employer .emobile_num").val();
            if(mobile=='')
            {
                alert('Please enter mobile number');
                return false;
            }else{
                otp(mobile);    
                return false;
            }
        });
        
        $(".otp").keyup(function(){
            var otp = $(this).val();
            var mobile = $(".mobile_num").val();
            if($(this).val().length > 3)
            {
                verifyotp(otp,mobile);
            }
        });
        
        $(".eotp").keyup(function(){
            var otp = $(this).val();
            var mobile = $(".emobile_num").val();
            if($(this).val().length > 3)
            {
                everifyotp(otp,mobile);
            }
        });
        
        
    });
    
    function verifyotp(otp,mobile)
    {
        
            $.ajax({  
                 url:"../otp.php",  
                 method:"POST",  
                 data:{'mobile':mobile,'action':'verify','otp':otp},  
                 success:function(data)  
                 {  
                     if(data =='OTP not matched')
                     {
                         $(".otp").val('');
                         $(".txtmsg").addClass("text-danger");
                         $(".txtmsg").text(data);
                         return false;
                     }else{
                         $(".txtmsg").removeClass("text-danger");
                         $(".txtmsg").addClass("text-success");
                         $(".txtmsg").text(data);
                         return false;
                         
                     }
                 }  
            });
      
    }
    
    function everifyotp(otp,mobile)
    {
        
            $.ajax({  
                 url:"../otp.php",  
                 method:"POST",  
                 data:{'mobile':mobile,'action':'verify','otp':otp},  
                 success:function(data)  
                 {  
                     if(data =='OTP not matched')
                     {
                         $(".eotp").val('');
                         $(".etxtmsg").addClass("text-danger");
                         $(".etxtmsg").text(data);
                         return false;
                     }else{
                         $(".etxtmsg").removeClass("text-danger");
                         $(".etxtmsg").addClass("text-success");
                         $(".etxtmsg").text(data);
                         return false;
                         
                     }
                 }  
            });
      
    }
    
    function otp(mobile)
    {
        $.ajax({  
             url:"../otp.php",  
             method:"POST",  
             data:{'mobile':mobile,'action':'otp'},  
             success:function(data)  
             {  if(data=='true')
                {
                    alert('Otp sent on entered mobile number');
                    return false;
                }else{
                    alert('Otp Sent Failed');
                    return false;
                }
                
             }  
        });  
    }
</script>
</body>

</html>