@foreach (session('flash_notification', collect())->toArray() as $message)
@if ($message['overlay'])
@include('flash::modal', [
'modalClass' => 'flash-modal',
'title'      => $message['title'],
'body'       => $message['message']
])
@else

    <div class="pop-alert">
        <div class="alert-body alert alert-dismissible
     alert-{{ $message['level'] }}
     {{ $message['important'] ? 'alert-important' : '' }}">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    @if ($message['important'])
    
    @endif
    {!! $message['message'] !!}
</div></div>
@endif
@endforeach
{{ session()->forget('flash_notification') }}
