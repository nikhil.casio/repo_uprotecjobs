<?php echo APFrmErrHelp::showErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">
    <h3>Drag and Drop to Sort Job Experiences</h3>
    <?php echo Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'refreshJobExperienceSortData();')); ?>

    <div id="jobExperienceSortDataDiv"></div>
</div>
<?php $__env->startPush('scripts'); ?> 
<script>
    $(document).ready(function () {
        refreshJobExperienceSortData();
    });
    function refreshJobExperienceSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('job.experience.sort.data')); ?>",
            data: {lang: language},
            success: function (responseData) {
                $("#jobExperienceSortDataDiv").html('');
                $("#jobExperienceSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var jobExperienceOrder = $(this).sortable('toArray').toString();
                        $.post("<?php echo e(route('job.experience.sort.update')); ?>", {jobExperienceOrder: jobExperienceOrder, _method: 'PUT', _token: '<?php echo e(csrf_token()); ?>'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
<?php $__env->stopPush(); ?>
