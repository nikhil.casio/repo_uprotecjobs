
<?php $__env->startSection('content'); ?> 
<!-- Header start --> 
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- Header end --> 
<!-- Inner Page Title start --> 
<?php echo $__env->make('includes.inner_page_title', ['page_title'=>__('Apply on Job')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container"> <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="userccount">
                    <div class="formpanel"> <?php echo Form::open(array('method' => 'post', 'route' => ['post.apply.job', $job_slug])); ?> 
                        <!-- Job Information -->
                        <h5><?php echo e($job->title); ?></h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="formrow<?php echo e($errors->has('cv_id') ? ' has-error' : ''); ?>"> <?php echo Form::select('cv_id', [''=>__('Select CV *')]+$myCvs, null, array('class'=>'form-control', 'id'=>'cv_id')); ?>

                                    <?php if($errors->has('cv_id')): ?> <span class="help-block"> <strong><?php echo e($errors->first('cv_id')); ?></strong> </span> <?php endif; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="formrow<?php echo e($errors->has('current_salary') ? ' has-error' : ''); ?>"> <?php echo Form::text('current_salary', null, array('class'=>'form-control', 'id'=>'current_salary', 'placeholder'=>__('Current salary').' ('.$job->getSalaryPeriod('salary_period').')' )); ?>

                                    <?php if($errors->has('current_salary')): ?> <span class="help-block"> <strong><?php echo e($errors->first('current_salary')); ?></strong> </span> <?php endif; ?> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="formrow<?php echo e($errors->has('expected_salary') ? ' has-error' : ''); ?>"> <?php echo Form::text('expected_salary', null, array('class'=>'form-control', 'id'=>'expected_salary', 'placeholder'=>__('Expected salary').' ('.$job->getSalaryPeriod('salary_period').')')); ?>

                                    <?php if($errors->has('expected_salary')): ?> <span class="help-block"> <strong><?php echo e($errors->first('expected_salary')); ?></strong> </span> <?php endif; ?> </div>
                            </div>
                            <div class="col-md-12">
                                <div class="formrow<?php echo e($errors->has('salary_currency') ? ' has-error' : ''); ?>"> <?php echo Form::text('salary_currency', Request::get('salary_currency', $siteSetting->default_currency_code), array('class'=>'form-control', 'id'=>'salary_currency', 'placeholder'=>__('Salary Currency'), 'autocomplete'=>'off')); ?>

                                    <?php if($errors->has('salary_currency')): ?> <span class="help-block"> <strong><?php echo e($errors->first('salary_currency')); ?></strong> </span> <?php endif; ?> </div>
                            </div>
                        </div>
                        <br>
                        <input type="submit" class="btn" value="<?php echo e(__('Apply on Job')); ?>">
                        <?php echo Form::close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?> 
<script>
    $(document).ready(function () {
        $('#salary_currency').typeahead({
            source: function (query, process) {
                return $.get("<?php echo e(route('typeahead.currency_codes')); ?>", {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });

    });


//code for only accept numeric moblie number

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $("#current_salary").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                // $(".errorjqr").css("display", ret ? "none" : "inline");
                return ret;
            });
            $("#current_salary").bind("paste", function (e) {
                return false;
            });
            $("#current_salary").bind("drop", function (e) {
                return false;
            });
        });


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $("#expected_salary").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                // $(".errorjqr").css("display", ret ? "none" : "inline");
                return ret;
            });
            $("#expected_salary").bind("paste", function (e) {
                return false;
            });
            $("#expected_salary").bind("drop", function (e) {
                return false;
            });
        });

</script> 
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>