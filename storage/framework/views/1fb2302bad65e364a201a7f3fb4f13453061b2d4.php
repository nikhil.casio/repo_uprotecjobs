<?php if($packages->count()): ?>
<div class="paypackages"> 
    <!---four-paln-->
    <div class="four-plan">
        <h3><?php echo e(__('Upgrade Package')); ?></h3>
        <div class="row"> <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <ul class="boxes">
                    <li class="plan-name"><?php echo e($package->package_title); ?></li>
                    <li>
                        <div class="main-plan text-center">
                            <div class="plan-price1-1">INR <span class="plan-price1-2"><?php echo e($package->package_price); ?></span></div>
                              <div class="gst-rates">+18%GST</div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <li class="plan-pages"><?php echo e(__('Can post jobs')); ?> : <?php echo e($package->package_num_listings); ?></li>
                    <li class="plan-pages"><?php echo e(__('Package Duration')); ?> : <?php echo e($package->package_num_days); ?> <?php echo e(__('Days')); ?></li>
                    <li class="plan-pages"><?php echo e($package->unlocking); ?></li>                    
                    <li class="plan-pages"><?php echo e($package->unlimited); ?></li>                    
                      <?php if($package->package_price == 5000): ?>
                     <li class="plan-pages">Active Post : Unlimited</li>
                     <?php else: ?>
                        <li class="plan-pages">Active Post : <?php echo e($package->total_active_post); ?></li>
                     <?php endif; ?>
                    
                   <?php if($package->package_price == 0): ?>
                    <li class="plan-pages">No Of Cities Selection : 1</li>
                    <li class="plan-pages">Best Of Hiring : Up to 3</li>
                    <li class="plan-pages">Candidate Database Unlocks : 100</li>
                    <li class="plan-pages">Priority Customer Support : NA</li>
                    <li class="plan-pages">Listing In Featured Jobs : NA</li>
                    <li class="plan-pages">Personalised Notification Of Your Job To Candidates : NA</li>
                    <?php endif; ?>
                    
                     <?php if($package->package_price == 2000): ?>
                    <li class="plan-pages">No Of Cities Selection : Unlimited</li>
                    <li class="plan-pages">Best Of Hiring : Up to 5</li>
                    <li class="plan-pages">Candidate Database Unlocks : Unlimited</li>
                    <li class="plan-pages">Priority Customer Support : NA</li>
                    <li class="plan-pages">Listing In Featured Jobs : NA</li>
                    <li class="plan-pages">Personalised Notification Of Your Job To Candidates : NA</li>
                    <?php endif; ?>
                    
                     <?php if($package->package_price == 5000): ?>
                    <li class="plan-pages">No Of Cities Selection : Unlimited</li>
                    <li class="plan-pages">Best Of Hiring : Up to 10</li>
                    <li class="plan-pages">Candidate Database Unlocks : Unlimited</li>
                    <li class="plan-pages">Priority Customer Support : NA</li>
                    <li class="plan-pages">Listing In Featured Jobs : NA</li>
                    <li class="plan-pages">Personalised Notification Of Your Job To Candidates : Yes</li>
                    <?php endif; ?>
                    
                    
                    <?php if((bool)$siteSetting->is_paypal_active): ?>
                    <li class="order paypal"><a href="<?php echo e(route('order.upgrade.package', $package->id)); ?>"><i class="fa fa-cc-paypal" aria-hidden="true"></i> <?php echo e(__('pay with paypal')); ?></a></li>
                    <?php endif; ?>
                    <?php if((bool)$siteSetting->is_stripe_active): ?>
                    <li class="order"><a href="<?php echo e(route('stripe.order.form', [$package->id, 'upgrade'])); ?>"><i class="fa fa-cc-stripe" aria-hidden="true"></i> <?php echo e(__('pay with stripe')); ?></a></li>
                    <?php endif; ?>
                    <li class="order"><a href="<?php echo e(route('payu.order.form', [$package->id, 'upgrade'])); ?>"><i class="fa fa-cc-stripe" aria-hidden="true"></i> <?php echo e(__('pay with Payumoney')); ?></a></li>
                </ul>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
    </div>
    <!---end four-paln--> 
</div>
<?php endif; ?>