<div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="userccount">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5><?php echo e(__('Update Education')); ?></h5>            
                        <div class="formpanel">
                            <div class="formrow">
                                <h3><?php echo e(__('Education Updated successfully')); ?></h3>
                            </div>                
                        </div>            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
