<?php echo APFrmErrHelp::showErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">
    <h3>Drag and Drop to Sort Career Levels</h3>
    <?php echo Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'refreshCareerLevelSortData();')); ?>

    <div id="careerLevelSortDataDiv"></div>
</div>
<?php $__env->startPush('scripts'); ?> 
<script>
    $(document).ready(function () {
        refreshCareerLevelSortData();
    });
    function refreshCareerLevelSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('career.level.sort.data')); ?>",
            data: {lang: language},
            success: function (responseData) {
                $("#careerLevelSortDataDiv").html('');
                $("#careerLevelSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var careerLevelOrder = $(this).sortable('toArray').toString();
                        $.post("<?php echo e(route('career.level.sort.update')); ?>", {careerLevelOrder: careerLevelOrder, _method: 'PUT', _token: '<?php echo e(csrf_token()); ?>'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
<?php $__env->stopPush(); ?>
