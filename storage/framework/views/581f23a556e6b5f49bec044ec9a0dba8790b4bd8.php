<?php echo APFrmErrHelp::showErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">
    <h3>Drag and Drop to Sort the Questions</h3>
    <?php echo Form::select('lang', ['' => '']+$languages, config('default_lang'), array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'refresh_faq_sort_data();')); ?>	
    <div id="faq_sort_data_div">
    </div>
</div>
<?php $__env->startPush('scripts'); ?> 
<script>
    $(document).ready(function () {
        refresh_faq_sort_data();
    });
    function refresh_faq_sort_data() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('faq.sort.data')); ?>",
            data: {lang: language},
            success: function (responseData) {
                $("#faq_sort_data_div").html('');
                $("#faq_sort_data_div").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var faqOrder = $(this).sortable('toArray').toString();
                        $.post("<?php echo e(route('faq.sort.update')); ?>", {faqOrder: faqOrder, _method: 'PUT', _token: '<?php echo e(csrf_token()); ?>'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script>
<?php $__env->stopPush(); ?>
