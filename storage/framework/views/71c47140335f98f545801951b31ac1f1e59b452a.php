<?php $__currentLoopData = session('flash_notification', collect())->toArray(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($message['overlay']): ?>
<?php echo $__env->make('flash::modal', [
'modalClass' => 'flash-modal',
'title'      => $message['title'],
'body'       => $message['message']
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>

    <div class="pop-alert">
        <div class="alert-body alert alert-dismissible
     alert-<?php echo e($message['level']); ?>

     <?php echo e($message['important'] ? 'alert-important' : ''); ?>">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php if($message['important']): ?>
    
    <?php endif; ?>
    <?php echo $message['message']; ?>

</div></div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php echo e(session()->forget('flash_notification')); ?>

