<?php if(!Auth::user() && !Auth::guard('company')->user()): ?>
<div class="emploginbox">
	<div class="container">		
		<div class="titleTop">
			<div class="subtitle"><?php echo e(__('Are You Looking For Candidates!')); ?></div>
           <h3><?php echo e(__('Post a Job Today')); ?>  </h3>
			<h4><?php echo e(__('“Hire right, because the penalties of hiring wrong are huge.” — Ray Dalio')); ?></h4>
        </div>
		<p>Are you struggling to find the right person for a job? Are your strategies failing in the execution mode? Don't compromise with an amateur, hire an expert.</p>
		<div class="viewallbtn"><a href="<?php echo e(route('register')); ?>"><?php echo e(__('Post a Job')); ?></a></div>
	</div>
</div>
<?php endif; ?>