

<?php $__env->startSection('content'); ?> 
<!-- Header start --> 
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- Header end --> 
<!-- Inner Page Title start --> 
<?php echo $__env->make('includes.inner_page_title', ['page_title'=>__('Pay with PayUmoney')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row"> 
            <?php if(Auth::guard('company')->check()): ?>
            <?php echo $__env->make('includes.company_dashboard_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php else: ?>
            <?php echo $__env->make('includes.user_dashboard_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
            <div class="col-md-9 col-sm-8">
                <div class="userccount">
                    <div class="row">
                        <div class="col-md-5">
                            <img src="<?php echo e(asset('/')); ?>images/payu.png" alt="" />
                            <div class="strippckinfo">
                                <h5><?php echo e(__('Invoice Details')); ?></h5>
                                <div class="pkginfo"><?php echo e(__('Package')); ?>: <strong><?php echo e($package->package_title); ?></strong></div>
                                <div class="pkginfo"><?php echo e(__('Price')); ?>: <strong>₹ <?php echo e($package->package_price); ?></strong></div>

                                <?php if(Auth::guard('company')->check()): ?>
                                <div class="pkginfo"><?php echo e(__('Can post jobs')); ?>: <strong><?php echo e($package->package_num_listings); ?></strong></div>
                                <?php else: ?>
                                <div class="pkginfo"><?php echo e(__('Can apply on jobs')); ?>: <strong><?php echo e($package->package_num_listings); ?></strong></div>
                                <?php endif; ?>
                                <div class="pkginfo"><?php echo e(__('Package Duration')); ?>: <strong><?php echo e($package->package_num_days); ?> <?php echo e(__('Days')); ?></strong></div>
                            </div>




                        </div>
                        <div class="col-md-7">
                            <div class="formpanel"> <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <h5><?php echo e(__('Payment by Payumoney')); ?></h5>
                                <?php                
                                $route = 'payu.order.form1';     
                                ?>                            
                                <form method="post" name="payuForm" action="https://secure.payu.in/_payment">          
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(Form::hidden('package_id', $package_id)); ?>

                                
                                <div class="row">
                                    
                                    <input class="form-control" name="package_price" value="<?php echo e($package->package_price); ?>" type="hidden">
                                    <input name="key" type="hidden" value="<?php echo $mkey ?>" />
                                    <input name="txnid" type="hidden"  value="<?php echo $tid ?>" />
                                    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                                    <input name="amount" type="hidden" value="<?php echo $amount; ?>" />
                                    <input name="productinfo" type="hidden" value="<?php echo $pinfo; ?>">
                                    <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                                    <input name="udf1" type="hidden" value="">
                                    <input name="udf2" type="hidden" value="">
                                    <input name="udf3" type="hidden" value="">
                                    <input name="udf4" type="hidden" value="">
                                    <input name="udf5" type="hidden" value="">
                                    <input name="firstname" id="firstname" type="hidden" value="<?php echo $name; ?>"/>
                                    <input name="email" id="email"  type="hidden"  value='<?php echo $mailid; ?>'>
                                    <input name="phone"   type="hidden"  value="<?php echo $phoneno; ?>">
                                    <input name="surl" type="hidden" value="http://uprotecjobs.com/jobs/public/success.php" size="64" />
                                    <input name="furl" type="hidden" value="http://uprotecjobs.com/jobs/public/success.php" size="64" />
                                    <input name="curl" type="hidden" value="http://uprotecjobs.com/jobs/public/success.php" />
                                        
                                    <div class="col-md-12">
                                        <div class="formrow">
                                            <button type="submit" class="btn"><?php echo e(__('Pay with Payu')); ?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('styles'); ?>
<style type="text/css">
    .userccount p{ text-align:left !important;}
</style>
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?> 
<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 
<script type="text/javascript">
Stripe.setPublishableKey('<?php echo e(Config::get('stripe.stripe_key')); ?>');
var $form = $('#stripe-form');
$form.submit(function (event) {
    $('#error_div').hide();
    $form.find('button').prop('disabled', true);
    Stripe.card.createToken({
        number: $('#card_no').val(),
        cvc: $('#cvvNumber').val(),
        exp_month: $('#ccExpiryMonth').val(),
        exp_year: $('#ccExpiryYear').val(),
        name: $('#card_name').val()
    }, stripeResponseHandler);
    return false;
});
function stripeResponseHandler(status, response) {
    if (response.error) {
        $('#error_div').show();
        $('#error_div').text(response.error.message);
        $form.find('button').prop('disabled', false);
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        // Submit the form:
        $form.get(0).submit();
    }
}
</script> 


<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>