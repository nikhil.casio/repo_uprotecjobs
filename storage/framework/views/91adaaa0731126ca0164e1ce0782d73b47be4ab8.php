
<?php $__env->startSection('content'); ?>
<!-- Header start -->
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Header end --> 
<!-- Inner Page Title start -->
<?php echo $__env->make('includes.inner_page_title', ['page_title'=>__('Job Applications')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row">
            <?php echo $__env->make('includes.company_dashboard_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="col-md-9 col-sm-8"> 
                <div class="myads">
                    <h3><?php echo e(__('Job Applications')); ?></h3>
                    <ul class="searchList">
                        <!-- job start --> 
                        <?php if(isset($job_applications) && count($job_applications)): ?>
                        <?php $__currentLoopData = $job_applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job_application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $user = $job_application->getUser();
                        $job = $job_application->getJob();
                        $company = $job->getCompany();             
                        $profileCv = $job_application->getProfileCv();
                        ?>
                        <?php if(null !== $job_application && null !== $user && null !== $job && null !== $company && null !== $profileCv): ?>
                        <li>
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <div class="jobimg"><?php echo e($user->printUserImage(100, 100)); ?></div>
                                    <div class="jobinfo">
                                        <h3><a href="<?php echo e(route('applicant.profile', $job_application->id)); ?>"><?php echo e($user->getName()); ?></a></h3>
                                        <div class="location"> <?php echo e($user->getLocation()); ?></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="minsalary"><?php echo e($job_application->expected_salary); ?> <?php echo e($job_application->salary_currency); ?> <span>/ <?php echo e($job->getSalaryPeriod('salary_period')); ?></span></div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="listbtn"><a href="<?php echo e(route('applicant.profile', $job_application->id)); ?>"><?php echo e(__('View Profile')); ?></a></div>
                                </div>
                            </div>
                            <p><?php echo e(str_limit($user->getProfileSummary('summary'),150,'...')); ?></p>
                        </li>
                        <!-- job end --> 
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>