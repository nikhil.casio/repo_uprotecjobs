<div class="container">
<div class="section howitwrap">
    
        <!-- title start -->
        <div class="titleTop">
            <h3><?php echo e(__('How It')); ?> <span><?php echo e(__('Works')); ?></span></h3>
        </div>
        <!-- title end -->
        <ul class="howlist row">
            <!--step 1-->
            <li class="col-md-4 col-sm-4">
                <div class="iconcircle"><i class="fa fa-user" aria-hidden="true"></i>
                </div>
                <h4><?php echo e(__('Create An Account')); ?></h4>
                <p><?php echo e(__('Your profile would be the first thing a prospective employer would notice. Make a lasting impression by creating a unique profile on Uprotec jobs.')); ?>.</p>
            </li>
            <!--step 1 end-->
            <!--step 2-->
            <li class="col-md-4 col-sm-4">
                <div class="iconcircle"><i class="fa fa-black-tie" aria-hidden="true"></i>
                </div>
                <h4><?php echo e(__('Search Desired Job')); ?></h4>
                <p><?php echo e(__('Sort through many job postings available on the site to find the ones that are right for you.')); ?>.</p>
            </li>
            <!--step 2 end-->
            <!--step 3-->
            <li class="col-md-4 col-sm-4">
                <div class="iconcircle"><i class="fa fa-file-text" aria-hidden="true"></i>
                </div>
                <h4><?php echo e(__('Send Your Resume')); ?></h4>
                <p><?php echo e(__('Yes you are almost done, let the employer know that you are interested.')); ?>.</p>
            </li>
            <!--step 3 end-->
        </ul>
    </div>
</div>