<div class="modal-dialog modal-lg">

    <div class="modal-content">

        <form class="form" id="add_edit_profile_cv" method="POST" action="<?php echo e(route('store.front.profile.cv', [$user->id])); ?>"><?php echo e(csrf_field()); ?>


            <input type="hidden" name="id" id="id" value="0"/>

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title"><?php echo e(__('Add CV')); ?></h4>

            </div>

            <?php echo $__env->make('user.forms.cv.cv_form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="modal-footer">

                <button type="button" class="btn btn-primary" onclick="submitProfileCvForm();"><?php echo e(__('Add CV')); ?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>

            </div>

        </form>

    </div>

    <!-- /.modal-content --> 

</div>

<!-- /.modal-dialog -->