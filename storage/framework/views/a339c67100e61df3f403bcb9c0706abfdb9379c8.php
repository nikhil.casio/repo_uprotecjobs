<!--Footer-->
<div class="largebanner shadow3">
    <div class="adin">
        <?php echo $siteSetting->above_footer_ad; ?>

    </div>
    <div class="clearfix"></div>
</div>


<div class="footerWrap">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-6">
                <div class="row">
                    <!--Quick Links-->
                    <div class="col-md-3 col-sm-6">
                        <h5>
                            <?php echo e(__('Job Seekers')); ?>

                            <span class="d-md-none footer-quicks" data-target="footer-job-seekers"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Quick Links menu Start-->
                        <ul class="quicklinks" id="footer-job-seekers">
                            <li><a href="<?php echo e(route('job.list')); ?>"><?php echo e(__('Search Job')); ?></a></li>
                            <li><a href="<?php echo e(route('blogs')); ?>"><?php echo e(__('Career Advice')); ?></a></li>
                            <li><a href="<?php echo e(route('job.list')); ?>?job_experience_id%5B%5D=11"><?php echo e(__('Jobs for Freshers')); ?></a></li>
                            <li><a href="<?php echo e(route('job.list')); ?>?job_experience_id%5B%5D=5"><?php echo e(__('Jobs for Experienced')); ?></a></li>
                        </ul>
                    </div>
                    <!--Quick Links menu end-->
                    <!--Quick Links-->
                    <div class="col-md-3 col-sm-6">
                        <h5>
                            <?php echo e(__('For Recruiters')); ?>

                            <span class="d-md-none footer-quicks" data-target="footer-for-recruiters"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Quick Links menu Start-->
                        <ul class="quicklinks" id="footer-for-recruiters">
                            <li><a href="<?php echo e(route('post.job')); ?>"><?php echo e(__('Post a Job')); ?></a></li>
                            <li><a href="<?php echo e(route('index')); ?>"><?php echo e(__('Search Candidates')); ?></a></li>
                            <li><a href="<?php echo e(route('index')); ?>"><?php echo e(__('Hiring Plans')); ?></a></li>
                            <li><a href="<?php echo e(route('index')); ?>"><?php echo e(__('Resume Database Access')); ?></a></li>
                        </ul>
                    </div>
                    <!--Quick Links menu end-->

                    <div class="col-md-3 col-sm-6">
                        <h5>
                            <?php echo e(__('Jobs By Functional Area')); ?>

                            <span class="d-md-none footer-quicks" data-target="footer-funct-area"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Quick Links menu Start-->
                        <ul class="quicklinks" id="footer-funct-area">
                            <?php
                            $functionalAreas = App\FunctionalArea::getUsingFunctionalAreas(10);
                            ?>
                            <?php $__currentLoopData = $functionalAreas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $functionalArea): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a
                                    href="<?php echo e(route('job.list', ['functional_area_id[]'=>$functionalArea->functional_area_id])); ?>"><?php echo e($functionalArea->functional_area); ?></a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>

                    <!--Jobs By Industry-->
                    <div class="col-md-3 col-sm-6">
                        <h5>
                            <?php echo e(__('Jobs By Industry')); ?>

                            <span class="d-md-none footer-quicks" data-target="footer-by-industry"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                        </h5>
                        <!--Industry menu Start-->
                        <ul class="quicklinks" id="footer-by-industry">
                            <?php
                            $industries = App\Industry::getUsingIndustries(10);
                            ?>
                            <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a
                                    href="<?php echo e(route('job.list', ['industry_id[]'=>$industry->industry_id])); ?>"><?php echo e($industry->industry); ?></a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                        <!--Industry menu End-->
                        <div class="clear"></div>
                    </div>
                        <div class="clear"></div>
                    </div>
            </div>


            <!--About Us-->
            <div class="col-md-3 col-sm-12">
                <h5><?php echo e(__('Contact Us')); ?></h5>
                <div class="address"><?php echo e($siteSetting->site_street_address); ?></div>
                <div class="email"> <a
                        href="mailto:<?php echo e($siteSetting->mail_to_address); ?>"><?php echo e($siteSetting->mail_to_address); ?></a> </div>
                <div class="phone"> <a
                        href="tel:<?php echo e($siteSetting->site_phone_primary); ?>"><?php echo e($siteSetting->site_phone_primary); ?></a>
                </div>
                <!-- Social Icons -->
                <div class="social"><?php echo $__env->make('includes.footer_social', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
                <!-- Social Icons end -->
            </div>
            <!--About us End-->


        </div>
    </div>
</div>
<!--Footer end-->
<!--Copyright-->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!--<div class="bttxt"><?php echo e(__('Copyright')); ?> &copy; <?php echo e(date('Y')); ?> <?php echo e($siteSetting->site_name); ?>. <?php echo e(__('All Rights Reserved')); ?>. <?php echo e(__('Design by')); ?>: <a href="<?php echo e(url('/')); ?>http://uprotecjobs.com/jobs" target="_blank">uprotecjob</a></div>-->
                <div class="bttxt"><?php echo e(__('Copyright')); ?> &copy; <?php echo e(date('Y')); ?> <?php echo e($siteSetting->site_name); ?>.
                    <?php echo e(__('All Rights Reserved')); ?>.</div>
            </div>
            <div class="col-md-4 text-right">
                <?php $__currentLoopData = $show_in_footer_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $footer_menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                $cmsContent = App\CmsContent::getContentBySlug($footer_menu->page_slug);
                ?>
                <a class="<?php echo e($footer_menu->page_slug); ?>" href="<?php echo e(route('cms', $footer_menu->page_slug)); ?>"><?php echo e($cmsContent->page_title); ?></a>&nbsp;&nbsp;&nbsp;
                
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div class="paylogos"><img src="<?php echo e(asset('/')); ?>images/payment-icons.png" alt="" /></div>
            </div>
        </div>

    </div>
</div>
<?php //print_r($_COOKIE); echo $_COOKIE['laravel_session'];?>
<?php //if($_COOKIE['laravel_session']=='') {?>
<div class="cookieConsentContainer">
    <div class="cookieTitle">
        <h3>Cookies</h3>
    </div>
    <div class="cookieDesc">
        <p>This website uses cookies to ensure you get the best experience on our website

        </p>
    </div>
    <div class="cookieButton">
        <a href="javascript:void(0)" class="button btn btn-primary cookieAcceptButton">Accept</a>
    </div>
</div>
<?php //} ?>
<script>
    document.addEventListener("DOMContentLoaded",function(){
        $(".footer-quicks").on("click",function(){
            var target = $(this).data("target");
            $("#"+target).toggle();
        })
    })
</script>