
<?php $__env->startSection('content'); ?>
<!-- Header start -->
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Header end -->
<!-- Inner Page Title start -->
<?php echo $__env->make('includes.inner_page_title', ['page_title'=>__('My Job Alerts')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row"> <?php echo $__env->make('includes.user_dashboard_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="col-lg-9 col-sm-8">
                <div class="userdashbox">
                    <h3><?php echo e(__('My Job Alerts')); ?></h3>
                   
						<table class="table">
						  <tbody>
							<tr>
							  <th scope="col">Alert Title</th>	
								<?php if(isset($id) && $id!=''): ?>
							  <th scope="col">Location</th>
								<?php endif; ?>
								<th scope="col">Created On</th>
							  <th scope="col">Action</th>
							</tr>							
							 <!-- job start -->
                        <?php if(isset($alerts) && count($alerts)): ?>
                        <?php $__currentLoopData = $alerts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $alert): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr id="delete_<?php echo e($alert->id); ?>">
                            <?php
                            if(null!==($alert->search_title)){
                            $title = $alert->search_title;
                            }

                            ?>
                            <?php
                            if(null!==($alert->country_id)){
                            $id = $alert->country_id;
                            }

                            if(isset($title) && $title!='' && isset($id) && $id!=''){
                            $cols = 'col-lg-4';
                            }else{
                            $cols = 'col-lg-8';
                            }
                            ?>
							
							<?php if(isset($title) && $title!=''): ?>
							<td><?php echo e($title); ?></td>
							<?php endif; ?>
                                <?php if(isset($id) && $id!=''): ?>
							  <td> <?php echo e($id); ?></td>
							<?php endif; ?>
							  <td> <?php echo e($alert->created_at->format('M d,Y')); ?></td>
							  <td> <a href="javascript:;" onclick="delete_alert(<?php echo e($alert->id); ?>)" class="delete_alert">Delete</a></td>							
                        </tr>
                        <!-- job end -->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?> 
							  
							  
						  </tbody>
						</table>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>
    <?php $__env->startPush('scripts'); ?>
    <script>
        function delete_alert(id) {

            $.ajax({
                type: 'GET',
                url: "<?php echo e(url('/')); ?>/delete-alert/" + id,
                success: function(response) {
                    if (response["status"] == true) {
                        $('#delete_' + id).hide();
                        swal({
                            title: "Success",
                            text: response["msg"],
                            icon: "success",
                            button: "OK",
                        });

                    } else {
                        swal({
                            title: "Already exist",
                            text: response["msg"],
                            icon: "error",
                            button: "OK",
                        });
                    }

                }
            });
        }
    </script>
    <?php echo $__env->make('includes.immediate_available_btn', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>