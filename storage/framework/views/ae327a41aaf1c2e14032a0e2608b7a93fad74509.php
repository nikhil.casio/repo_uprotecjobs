<?php echo APFrmErrHelp::showErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">        
    <?php echo Form::hidden('id', null); ?>

    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'country_id'); ?>">
        <?php echo Form::label('country_id', 'Country', ['class' => 'bold']); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'sort_name'); ?>">
        <?php echo Form::label('sort_name', 'Sort Name', ['class' => 'bold']); ?>

        <?php echo Form::text('sort_name', null, array('class'=>'form-control', 'id'=>'sort_name', 'placeholder'=>'Sort Name')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'sort_name'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'phone_code'); ?>">
        <?php echo Form::label('phone_code', 'Phone Code', ['class' => 'bold']); ?>

        <?php echo Form::text('phone_code', null, array('class'=>'form-control', 'id'=>'phone_code', 'placeholder'=>'Phone Code')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'phone_code'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'currency'); ?>">
        <?php echo Form::label('currency', 'Currency', ['class' => 'bold']); ?>

        <?php echo Form::text('currency', null, array('class'=>'form-control', 'id'=>'currency', 'placeholder'=>'Currency')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'currency'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'code'); ?>">
        <?php echo Form::label('code', 'Code', ['class' => 'bold']); ?>

        <?php echo Form::text('code', null, array('class'=>'form-control', 'id'=>'code', 'placeholder'=>'Code')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'code'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'symbol'); ?>">
        <?php echo Form::label('symbol', 'Symbol', ['class' => 'bold']); ?>

        <?php echo Form::text('symbol', null, array('class'=>'form-control', 'id'=>'symbol', 'placeholder'=>'Symbol')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'symbol'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'thousand_separator'); ?>">
        <?php echo Form::label('thousand_separator', 'Thousand Separator', ['class' => 'bold']); ?>

        <?php echo Form::text('thousand_separator', null, array('class'=>'form-control', 'id'=>'thousand_separator', 'placeholder'=>'Thousand Separator')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'thousand_separator'); ?>

    </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'decimal_separator'); ?>">
        <?php echo Form::label('decimal_separator', 'Decimal Separator', ['class' => 'bold']); ?>

        <?php echo Form::text('decimal_separator', null, array('class'=>'form-control', 'id'=>'decimal_separator', 'placeholder'=>'Decimal Separator')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'decimal_separator'); ?>

    </div>	
    <div class="form-actions">
        <?php echo Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')); ?>

    </div>
</div>
