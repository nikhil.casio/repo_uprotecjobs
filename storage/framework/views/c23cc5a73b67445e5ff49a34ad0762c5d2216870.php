<?php echo APFrmErrHelp::showOnlyErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'package_title'); ?>"> <?php echo Form::label('package_title', 'Package Title', ['class' => 'bold']); ?>

        <?php echo Form::text('package_title', null, array('class'=>'form-control', 'id'=>'package_title', 'placeholder'=>'Package Title')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'package_title'); ?> </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'package_price'); ?>"> <?php echo Form::label('package_price', 'Package Price(In INR)', ['class' => 'bold']); ?>

        <?php echo Form::text('package_price', null, array('class'=>'form-control', 'id'=>'package_price', 'placeholder'=>'Package Price')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'package_price'); ?> </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'package_num_days'); ?>"> <?php echo Form::label('package_num_days', 'Package num days', ['class' => 'bold']); ?>

        <?php echo Form::text('package_num_days', null, array('class'=>'form-control', 'id'=>'package_num_days', 'placeholder'=>'Package num days')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'package_num_days'); ?> </div>
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'package_num_listings'); ?>"> <?php echo Form::label('package_num_listings', 'Package num listings*', ['class' => 'bold']); ?>

        <?php echo Form::text('package_num_listings', null, array('class'=>'form-control', 'id'=>'package_num_listings', 'placeholder'=>'Package num listings')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'package_num_listings'); ?> 
    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'total_active_post'); ?>"> <?php echo Form::label('total_active_post', 'Total Active Post*', ['class' => 'bold']); ?>

        <?php echo Form::text('total_active_post', null, array('class'=>'form-control', 'id'=>'total_active_post', 'placeholder'=>'Total Active Post')); ?>

        <?php echo APFrmErrHelp::showErrors($errors, 'total_active_post'); ?> 
        *On how many jobs a job seeker can apply<br />
        **How many jobs an employer can post </div>

    <div class="form-group <?php echo APFrmErrHelp::hasError($errors, 'package_for'); ?>">
        <?php echo Form::label('package_for', 'Package for?', ['class' => 'bold']); ?>

        <div class="radio-list">
            <?php
            $package_for_1 = 'checked="checked"';
            $package_for_2 = '';
            if (old('package_for', ((isset($package)) ? $package->package_for : 'job_seeker')) == 'employer') {
                $package_for_1 = '';
                $package_for_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="job_seeker" name="package_for" type="radio" value="job_seeker" <?php echo e($package_for_1); ?>>
                Job Seeker </label>
            <label class="radio-inline">
                <input id="employer" name="package_for" type="radio" value="employer" <?php echo e($package_for_2); ?>>
                Employer </label>
        </div>
        <?php echo APFrmErrHelp::showErrors($errors, 'package_for'); ?>

    </div>
    <div class="form-actions"> <?php echo Form::button('Update <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')); ?> </div>
</div>