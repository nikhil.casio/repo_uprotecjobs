<div class="header position-sticky">
    <div class="container-fluid">
        <div class="row align-items-center nav-contact d-none d-lg-flex d-xl-flex">
            <div class="col-lg-10 col-md-8 col-12">
                <ul class="top-bar-details nav navbar-nav top_nav flex-row">
                    <li><a href="#"><i class="fa fa-phone"></i> <?php echo e($siteSetting->site_phone_secondary); ?></a></li>
                    <li><a href="#"><i class="fa fa-mobile"></i> <?php echo e($siteSetting->site_phone_primary); ?></a></li>
                    <li><a href="mailto:response@uprotecjobs.com"><i class="fa fa-envelope"></i> <?php echo e($siteSetting->mail_to_address); ?></a></li>
            
                </ul>
            </div>
            <div class="col-lg-2 col-md-4 col-12 text-center headsocial">
                <?php echo $__env->make('includes.footer_social', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
        <div class="row align-items-center menu-bg1">
            <div class="col-lg-2 col-md-12 col-12"> <a href="<?php echo e(url('/')); ?>" class="logo"><img src="<?php echo e(asset('/')); ?>sitesetting_images/thumb/<?php echo e($siteSetting->site_logo); ?>" alt="<?php echo e($siteSetting->site_name); ?>"/></a>
                <div class="navbar-header navbar-light">
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav-main" aria-controls="nav-main" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-10 col-md-12 col-12"> 
                <!-- Nav start -->
                <nav class="navbar navbar-expand-lg navbar-light">
					
                    <div class="navbar-collapse collapse" id="nav-main">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item <?php echo e(Request::url() == route('index') ? 'active' : ''); ?>"><a href="<?php echo e(url('/')); ?>" class="nav-link"><?php echo e(__('Home')); ?></a> </li>
							
                            
							<?php if(Auth::guard('company')->check()): ?>
							<li class="nav-item"><a href="<?php echo e(url('/job-seekers')); ?>" class="nav-link"><?php echo e(__('Seekers')); ?></a> </li>
							<?php else: ?>
							<li class="nav-item"><a href="<?php echo e(url('/jobs')); ?>" class="nav-link"><?php echo e(__('Jobs')); ?></a> </li>
							<?php endif; ?>
							<li class="nav-item <?php echo e(Request::url()); ?>"><a href="<?php echo e(url('/companies')); ?>" class="nav-link"><?php echo e(__('Companies')); ?></a> </li>
                            <?php $__currentLoopData = $show_in_top_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $top_menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php $cmsContent = App\CmsContent::getContentBySlug($top_menu->page_slug); ?>
                            <li class="nav-item <?php echo e(Request::url() == route('cms', $top_menu->page_slug) ? 'active' : ''); ?>"><a href="<?php echo e(route('cms', $top_menu->page_slug)); ?>" class="nav-link"><?php echo e($cmsContent->page_title); ?></a> </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<li class="nav-item <?php echo e(Request::url() == route('blogs') ? 'active' : ''); ?>"><a href="<?php echo e(route('blogs')); ?>" class="nav-link"><?php echo e(__('Blog')); ?></a> </li>
                            <li class="nav-item <?php echo e(Request::url() == route('contact.us') ? 'active' : ''); ?>"><a href="<?php echo e(route('contact.us')); ?>" class="nav-link"><?php echo e(__('Contact us')); ?></a> </li>
                            <?php if(Auth::check()): ?>
                            <li class="nav-item dropdown userbtn"><a href="javascript:void(0);"><?php echo e(Auth::user()->printUserImage()); ?></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a href="<?php echo e(route('home')); ?>" class="nav-link"><i class="fa fa-tachometer" aria-hidden="true"></i> <?php echo e(__('Dashboard')); ?></a> </li>
                                    <li class="nav-item"><a href="<?php echo e(route('my.profile')); ?>" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> <?php echo e(__('My Profile')); ?></a> </li>
                                    <li class="nav-item"><a href="<?php echo e(route('view.public.profile', Auth::user()->id)); ?>" class="nav-link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo e(__('View Public Profile')); ?></a> </li>
                                    <li><a href="<?php echo e(route('my.job.applications')); ?>" class="nav-link"><i class="fa fa-desktop" aria-hidden="true"></i> <?php echo e(__('My Job Applications')); ?></a> </li>
                                    <li class="nav-item"><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();" class="nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo e(__('Logout')); ?></a> </li>
                                    <form id="logout-form-header" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo e(csrf_field()); ?>

                                    </form>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(Auth::guard('company')->check()): ?>
                            <li class="nav-item postjob"><a href="<?php echo e(route('post.job')); ?>" class="nav-link register"><?php echo e(__('Post a job')); ?></a> </li>
                            <li class="nav-item dropdown userbtn"><a href=""><?php echo e(Auth::guard('company')->user()->printCompanyImage()); ?></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a href="<?php echo e(route('company.home')); ?>" class="nav-link"><i class="fa fa-tachometer" aria-hidden="true"></i> <?php echo e(__('Dashboard')); ?></a> </li>
                                    <li class="nav-item"><a href="<?php echo e(route('company.profile')); ?>" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> <?php echo e(__('Company Profile')); ?></a></li>
                                    <li class="nav-item"><a href="<?php echo e(route('post.job')); ?>" class="nav-link"><i class="fa fa-desktop" aria-hidden="true"></i> <?php echo e(__('Post Job')); ?></a></li>
                                    <li class="nav-item"><a href="<?php echo e(route('company.messages')); ?>" class="nav-link"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo e(__('Company Messages')); ?></a></li>
                                    <li class="nav-item"><a href="<?php echo e(route('company.logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();" class="nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo e(__('Logout')); ?></a> </li>
                                    <form id="logout-form-header1" action="<?php echo e(route('company.logout')); ?>" method="POST" style="display: none;">
                                        <?php echo e(csrf_field()); ?>

                                    </form>
                                </ul>
                            </li>
                            <?php endif; ?> <?php if(!Auth::user() && !Auth::guard('company')->user()): ?>
                            <li class="nav-item"><a href="<?php echo e(route('login')); ?>" class="nav-link"><?php echo e(__('Sign in')); ?></a> </li>
							<li class="nav-item"><a href="<?php echo e(route('register')); ?>" class="nav-link register"><?php echo e(__('Register')); ?></a> </li>                            
                            <?php endif; ?>
                        </ul>
                    </div>
				</nav>
			</div>
		</div>
	</div>
</div>