<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form class="form" id="add_edit_profile_cv" method="PUT" action="<?php echo e(route('update.front.profile.cv', [$profileCv->id, $user->id])); ?>">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id" id="id" value="<?php echo e($profileCv->id); ?>"/>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo e(__('Edit CV')); ?></h4>
            </div>
            <?php echo $__env->make('user.forms.cv.cv_form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-large btn-primary" onClick="submitProfileCvForm();"><?php echo e(__('Update CV')); ?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
            </div>
        </form>
    </div>
    <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog -->