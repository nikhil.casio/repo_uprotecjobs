<?php echo APFrmErrHelp::showErrorsNotice($errors); ?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-body">
    <h3>Drag and Drop to Sort Salary Periods</h3>
    <?php echo Form::select('lang', ['' => 'Select Language']+$languages, config('default_lang'), array('class'=>'form-control', 'id'=>'lang', 'onchange'=>'refreshSalaryPeriodSortData();')); ?>

    <div id="salaryPeriodSortDataDiv"></div>
</div>
<?php $__env->startPush('scripts'); ?> 
<script>
    $(document).ready(function () {
        refreshSalaryPeriodSortData();
    });
    function refreshSalaryPeriodSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('salary.period.sort.data')); ?>",
            data: {lang: language},
            success: function (responseData) {
                $("#salaryPeriodSortDataDiv").html('');
                $("#salaryPeriodSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var salaryPeriodOrder = $(this).sortable('toArray').toString();
                        $.post("<?php echo e(route('salary.period.sort.update')); ?>", {salaryPeriodOrder: salaryPeriodOrder, _method: 'PUT', _token: '<?php echo e(csrf_token()); ?>'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
<?php $__env->stopPush(); ?>
