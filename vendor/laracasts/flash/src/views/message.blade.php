@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        <div class="pop-alert alert
                    alert-{{ $message['level'] }}
                    {{ $message['important'] ? 'alert-important' : '' }} alert-dismissible fade in" id="pop-success-alert"
                    role="alert"
        >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<i class="fa fa-times"></i>
            @if ($message['important'])
                <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true"
                >&times;</button>
            @endif

            {!! $message['message'] !!}
        </div>
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
